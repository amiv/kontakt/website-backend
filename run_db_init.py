import pytz
import datetime

from kontaktweb import app
from kontaktweb.models import User, Company, Fair, BoothOption, PackageOption

if __name__ == '__main__':
    with app.app_context():
        timezone = pytz.timezone('Europe/Zurich')

        company = Company()
        company.company_name = "AMIV Kontakt Team"
        company.billing_address_street = "Universitätstrasse 6"
        company.billing_address_postal_code = "8092"
        company.billing_address_city = "Zürich"
        company.billing_address_country = "Switzerland"
        Company.persist(company)

        user = User()
        user.company_id = company.id
        user.first_name = "Admin"
        user.last_name = "Istrator"
        user.email_address = "admin@amiv.ethz.ch"
        user.set_password("admin")
        User.persist(user)

        fair = Fair()
        fair.kontakt_title = "Kontakt.24"
        fair.kontakt_president = "cwalter"
        fair.kontakt_treasurer = "cwalter"
        fair.is_current = True
        fair.date_start = datetime.date(2021, 10, 12)
        fair.date_end = datetime.date(2021, 10, 14)
        fair.number_of_days = 3
        fair.registration_start = datetime.datetime(2021, 3, 2, hour=8, tzinfo=timezone)
        fair.registration_end = datetime.datetime(2021, 3, 31, hour=20, tzinfo=timezone)
        fair.profile_deadline = datetime.datetime(2021, 7, 11, tzinfo=timezone)
        fair.fairguide_changes_deadline = datetime.datetime(2021, 8, 8, tzinfo=timezone)
        fair.startup_discount = 300
        Fair.persist(fair)

        booth_option_a = BoothOption()
        booth_option_a.fair_id = fair.id
        booth_option_a.booth_category = "A"
        booth_option_a.day_one_spots = 15
        booth_option_a.day_two_spots = 15
        booth_option_a.price_one_day_small = 1200
        booth_option_a.price_two_days_small = 2500
        booth_option_a.price_one_day_large = 1500
        booth_option_a.price_two_days_large = 3500
        BoothOption.persist(booth_option_a)

        booth_option_b = BoothOption()
        booth_option_b.fair_id = fair.id
        booth_option_b.booth_category = "B"
        booth_option_b.day_one_spots = 10
        booth_option_b.day_two_spots = 10
        booth_option_b.price_one_day_small = 1100
        booth_option_b.price_two_days_small = 2300
        booth_option_b.price_one_day_large = 1400
        booth_option_b.price_two_days_large = 3200
        BoothOption.persist(booth_option_b)

        booth_option_c = BoothOption()
        booth_option_c.fair_id = fair.id
        booth_option_c.booth_category = "s"
        booth_option_c.day_one_spots = 6
        booth_option_c.day_two_spots = 6
        booth_option_c.price_one_day_small = 600
        booth_option_c.price_two_days_small = 900
        booth_option_c.price_one_day_large = None
        booth_option_c.price_two_days_large = None
        BoothOption.persist(booth_option_c)

        package_option_1 = PackageOption()
        package_option_1.fair_id = fair.id
        package_option_1.title_de = "Mediapaket"
        package_option_1.title_en = "Media Package"
        package_option_1.description_de = "Eine Werbeseite neben ihrem Portrait im Messeführer"
        package_option_1.description_en = "One-page advertisement next to your profile in the fair guide"
        package_option_1.price = 400
        package_option_1.multiple = True
        package_option_1.price_multiple = 300
        package_option_1.includes_ad = True
        PackageOption.persist(package_option_1)

        package_option_2 = PackageOption()
        package_option_2.fair_id = fair.id
        package_option_2.title_de = "Business Paket"
        package_option_2.title_en = "Business Package"
        package_option_2.description_de = "Als Partner auf der Kontakt Webseite aufgeführt."
        package_option_2.description_en = "Mention as partner on the home page on the Kontakt."
        package_option_2.price = 1000
        package_option_1.multiple = False
        package_option_2.includes_ad = False
        PackageOption.persist(package_option_2)

        package_option_3 = PackageOption()
        package_option_3.fair_id = fair.id
        package_option_3.title_de = "Premium Paket"
        package_option_3.title_en = "Premium Package"
        package_option_3.description_de = "Alles vom Business Paket."
        package_option_3.description_en = "Everything in the Business package."
        package_option_3.price = 2500
        package_option_3.mutex_package_id = package_option_2.id
        package_option_3.includes_ad = False
        PackageOption.persist(package_option_3)

        package_option_2.mutex_package_id = package_option_3.id
        PackageOption.persist(package_option_2)
