FROM python:3.7-alpine

# Create user with home directory and no password and change workdir
RUN adduser -S kontaktwebsite
WORKDIR /kontaktwebsite
# Run on port 8080 (does not require privilege to bind)
EXPOSE 8080
# Environment variable for config
ENV KONTAKTWEB_CONFIG=/kontaktwebsite/secrets.py

# Install bjoern and dependencies for install
RUN apk add --no-cache --virtual .deps \
        musl-dev gcc git && \
    # Keep libev for running bjoern
    apk add --no-cache libev-dev && \
    pip install bjoern

# Install pip requirements and (install) dependencies
RUN apk add --no-cache openssl-dev libffi-dev
COPY ./requirements.txt /kontaktwebsite/requirements.txt
RUN pip install -r /kontaktwebsite/requirements.txt

# Clean up install dependencies
RUN apk del .deps

# Copy other files
COPY ./ /kontaktwebsite

# Update permissions for entrypoint
RUN chmod 755 entrypoint.sh

# Switch user
USER kontaktwebsite

# Start application
CMD [ "./entrypoint.sh" ]
