"""Barebone setup, just enough to install."""

from setuptools import find_packages, setup

setup(
    name='kontakt-website-backend',
    version='1.0.0',
    packages=find_packages(),
)
