"""empty message

Revision ID: e0c5ad76210f
Revises: 006eb72b638d
Create Date: 2020-08-15 14:50:11.209804

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e0c5ad76210f'
down_revision = '006eb72b638d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('usertokens',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column('token', sa.String(length=32), nullable=False),
                    sa.Column('expire_date', sa.DateTime(), nullable=False),
                    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_usertokens_token'), 'usertokens', ['token'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_usertokens_token'), table_name='usertokens')
    op.drop_table('usertokens')
    # ### end Alembic commands ###
