"""Restructuring database schema

Revision ID: 842f7cb1bdf4
Revises: e0c5ad76210f
Create Date: 2021-02-10 15:14:58.363521

"""
import enum
import json
from datetime import datetime
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
from sqlalchemy import orm, Float, Integer, Numeric, Enum, Boolean, Date, DateTime, String, UnicodeText
from sqlalchemy.ext.declarative import declarative_base

# revision identifiers, used by Alembic.
revision = '842f7cb1bdf4'
down_revision = 'e0c5ad76210f'
branch_labels = None
depends_on = None


# Models for data migration
Base = declarative_base()


class BoothSize(enum.Enum):
    SMALL = 0
    LARGE = 1


class FairDays(enum.Enum):
    FIRST = 1
    SECOND = 2
    BOTH = 3


class Fair(Base):
    __tablename__ = 'fairs'

    id = sa.Column(Integer, primary_key=True)
    cv_check_company_id = sa.Column(Integer, sa.ForeignKey('companies.id'), nullable=True)
    date_one = sa.Column(Date, nullable=False)
    date_two = sa.Column(Date, nullable=False)
    registration_start = sa.Column(DateTime, nullable=False)
    registration_end = sa.Column(DateTime, nullable=False)

    # old fields
    dates = sa.Column(String(100), nullable=False)
    registration_period = sa.Column(String(100), nullable=False)


class Company(Base):
    __tablename__ = 'companies'

    id = sa.Column(Integer, primary_key=True)

    # Intentionally no default specified in the schema. DateTime should only be filled out
    # by application and not by the database server itself.
    created_time = sa.Column(DateTime, nullable=False)
    updated_time = sa.Column(DateTime, nullable=False)

    # old fields
    is_cv_check_company = sa.Column(Boolean, nullable=False, default=False)
    is_career_center = sa.Column(Boolean, nullable=False, default=False)


class Participation(Base):
    __tablename__ = 'participations'

    id = sa.Column(Integer, primary_key=True)
    company_id = sa.Column(Integer, sa.ForeignKey('companies.id'), nullable=False)
    fair_id = sa.Column(Integer, sa.ForeignKey('fairs.id'), nullable=False)
    time_of_signup = sa.Column(DateTime, nullable=False)
    created_time = sa.Column(DateTime, nullable=False)
    updated_time = sa.Column(DateTime, nullable=False)

    # old fields
    booth_price = sa.Column(Float, nullable=False)

    booths_booked = orm.relationship('BoothBooked')
    company = orm.relationship('Company')
    fair = orm.relationship('Fair')


class BoothOption(Base):
    __tablename__ = 'booth_options'

    id = sa.Column(Integer, primary_key=True)
    price_one_day_small = sa.Column(Numeric(10, 2), nullable=False)
    price_two_days_small = sa.Column(Numeric(10, 2), nullable=False)
    price_one_day_large = sa.Column(Numeric(10, 2), nullable=False)
    price_two_days_large = sa.Column(Numeric(10, 2), nullable=False)

    def get_booth_price(self, day, size):
        """Returns the price of the booth with the given day(s) and size (as enums)."""
        if day == FairDays.FIRST or day == FairDays.SECOND:
            if size == BoothSize.SMALL:
                return self.price_one_day_small
            else:
                return self.price_one_day_large
        elif day == FairDays.BOTH:
            if size == BoothSize.SMALL:
                return self.price_two_days_small
            else:
                return self.price_two_days_large
        raise ValueError


class BoothBooked(Base):
    __tablename__ = 'booths_booked'

    id = sa.Column(Integer, primary_key=True)
    participation_id = sa.Column(Integer, sa.ForeignKey('participations.id'), nullable=False)
    booth_id = sa.Column(Integer, sa.ForeignKey('booth_options.id'), nullable=False)
    day = sa.Column(Enum(FairDays), nullable=False)
    booth_size = sa.Column(Enum(BoothSize), nullable=False)
    message = sa.Column(UnicodeText)
    booth_location = sa.Column(String(100), nullable=True)
    price = sa.Column(Numeric(10, 2), nullable=False)

    booth_option = orm.relationship('BoothOption')


class CompanyProfile(Base):
    __tablename__ = 'company_profiles'

    id = sa.Column(Integer, primary_key=True)
    participation_id = sa.Column(Integer, sa.ForeignKey('participations.id'), nullable=False)

    # Intentionally no default specified in the schema. DateTime should only be filled out
    # by application and not by the database server itself.
    created_time = sa.Column(DateTime, nullable=False)
    updated_time = sa.Column(DateTime, nullable=False)

    # old fields
    fair_id = sa.Column(Integer, sa.ForeignKey('fairs.id'), nullable=False)
    company_id = sa.Column(Integer, sa.ForeignKey('companies.id'), nullable=False)

    fair = orm.relationship('Fair')
    company = orm.relationship('Company')


def upgrade():
    op.alter_column('booth_options', 'price_one_day_small', type=sa.Numeric(precision=10, scale=2),
                    existing_nullable=False)
    op.alter_column('booth_options', 'price_two_days_small', type=sa.Numeric(precision=10, scale=2),
                    existing_nullable=False)
    op.alter_column('booth_options', 'price_one_day_large', type=sa.Numeric(precision=10, scale=2),
                    existing_nullable=False)
    op.alter_column('booth_options', 'price_two_days_large', type=sa.Numeric(precision=10, scale=2),
                    existing_nullable=False)
    op.alter_column('package_options', 'price', type=sa.Numeric(precision=10, scale=2), existing_nullable=False)
    op.alter_column('fairs', 'startup_discount', type=sa.Numeric(precision=10, scale=2), existing_nullable=False)

    op.add_column('booths_booked', sa.Column('price', sa.Numeric(precision=10, scale=2), nullable=False))
    op.add_column('companies', sa.Column('created_time', sa.DateTime(), nullable=True))
    op.add_column('companies', sa.Column('updated_time', sa.DateTime(), nullable=True))
    op.add_column('company_profiles', sa.Column('created_time', sa.DateTime(), nullable=True))
    op.add_column('company_profiles', sa.Column('updated_time', sa.DateTime(), nullable=True))
    op.add_column('company_profiles', sa.Column('participation_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'company_profiles', 'participations', ['participation_id'], ['id'])
    op.add_column('fairs', sa.Column('cv_check_company_id', sa.Integer(), nullable=True))
    op.add_column('fairs', sa.Column('date_one', sa.Date(), nullable=True))
    op.add_column('fairs', sa.Column('date_two', sa.Date(), nullable=True))
    op.add_column('fairs', sa.Column('registration_end', sa.DateTime(), nullable=True))
    op.add_column('fairs', sa.Column('registration_start', sa.DateTime(), nullable=True))
    op.create_foreign_key(None, 'fairs', 'companies', ['cv_check_company_id'], ['id'])
    op.add_column('participations', sa.Column('created_time', sa.DateTime(), nullable=True))
    op.add_column('participations', sa.Column('updated_time', sa.DateTime(), nullable=True))

    # Migrate existing data
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    for fair in session.query(Fair):
        dates = json.loads(fair.dates)
        fair.date_one = datetime.fromisoformat(dates[0])
        fair.date_two = datetime.fromisoformat(dates[1])
        registration_period = json.loads(fair.registration_period)
        fair.registration_start = datetime.fromisoformat(registration_period[0])
        fair.registration_end = datetime.fromisoformat(registration_period[1])

        session.add(fair)

    for participation in session.query(Participation):
        participation.created_time = participation.time_of_signup
        participation.updated_time = participation.time_of_signup

        if participation.company.is_cv_check_company:
            participation.fair.cv_check_company_id = participation.company.id

        if participation.company.created_time is None:
            participation.company.created_time = participation.time_of_signup
            participation.company.updated_time = participation.company.updated_time
            session.add(participation.company)

        amount_left = participation.booth_price

        for booth_booked in participation.booths_booked:
            if booth_booked.day == FairDays.BOTH:
                if booth_booked.booth_size == BoothSize.SMALL:
                    regular_price = booth_booked.booth_option.price_two_days_small
                else:
                    regular_price = booth_booked.booth_option.price_two_days_large
            else:
                if booth_booked.booth_size == BoothSize.SMALL:
                    regular_price = booth_booked.booth_option.price_one_day_small
                else:
                    regular_price = booth_booked.booth_option.price_one_day_large
            booth_booked.price = min(amount_left, regular_price)
            amount_left = max(0, amount_left - booth_booked.price)
            session.add(booth_booked)

        if amount_left > 0:
            booth_booked = participation.booths_booked[0]
            booth_booked.price = booth_booked.price + amount_left

        session.add(participation)

    for company in session.query(Company).filter_by(created_time=None):
        company.created_time = datetime.now()
        company.updated_time = company.created_time
        session.add(company)

    for company_profile in session.query(CompanyProfile):
        participation = session.query(Participation).filter_by(fair_id=company_profile.fair_id,
                                                               company_id=company_profile.company_id).first()
        if participation is None:
            # Orphaned company profile found. Get rid of it!
            session.delete(company_profile)
        else:
            company_profile.participation_id = participation.id
            company_profile.created_time = participation.created_time
            company_profile.updated_time = participation.updated_time
            session.add(company_profile)

    session.commit()

    # Finalize column configurations
    op.alter_column('companies', 'created_time', existing_type=sa.DateTime(), nullable=False)
    op.alter_column('companies', 'updated_time', existing_type=sa.DateTime(), nullable=False)
    op.alter_column('company_profiles', 'created_time', existing_type=sa.DateTime(), nullable=False)
    op.alter_column('company_profiles', 'updated_time', existing_type=sa.DateTime(), nullable=False)
    op.alter_column('company_profiles', 'participation_id', existing_type=sa.Integer(), nullable=False)
    op.alter_column('fairs', 'date_one', existing_type=sa.Date(), nullable=False)
    op.alter_column('fairs', 'date_two', existing_type=sa.Date(), nullable=False)
    op.alter_column('fairs', 'registration_end', existing_type=sa.DateTime(), nullable=False)
    op.alter_column('fairs', 'registration_start', existing_type=sa.DateTime(), nullable=False)
    op.alter_column('participations', 'created_time', existing_type=sa.DateTime(), nullable=False)
    op.alter_column('participations', 'updated_time', existing_type=sa.DateTime(), nullable=False)

    # Cleanup database schema
    op.drop_column('companies', 'is_cv_check_company')
    op.drop_column('companies', 'is_career_center')
    op.drop_constraint('company_profiles_ibfk_1', 'company_profiles', type_='foreignkey')
    op.drop_constraint('company_profiles_ibfk_2', 'company_profiles', type_='foreignkey')
    op.drop_column('company_profiles', 'company_id')
    op.drop_column('company_profiles', 'fair_id')
    op.drop_column('fairs', 'dates')
    op.drop_column('fairs', 'is_last')
    op.drop_column('fairs', 'registration_period')
    op.drop_column('participations', 'booth_price')


def downgrade():
    op.alter_column('package_options', 'price', type=mysql.FLOAT(), existing_nullable=False)
    op.alter_column('booth_options', 'price_one_day_small', type=mysql.FLOAT(), existing_nullable=False)
    op.alter_column('booth_options', 'price_two_days_small', type=mysql.FLOAT(), existing_nullable=False)
    op.alter_column('booth_options', 'price_one_day_large', type=mysql.FLOAT(), existing_nullable=False)
    op.alter_column('booth_options', 'price_two_days_large', type=mysql.FLOAT(), existing_nullable=False)
    op.alter_column('fairs', 'startup_discount', type=mysql.FLOAT(), existing_nullable=False)

    op.add_column('participations', sa.Column('booth_price', mysql.FLOAT(), nullable=False))
    op.add_column('fairs', sa.Column('registration_period', mysql.VARCHAR(length=100), nullable=False))
    op.add_column('fairs', sa.Column('is_last', mysql.TINYINT(display_width=1), autoincrement=False, nullable=False))
    op.add_column('fairs', sa.Column('dates', mysql.VARCHAR(length=100), nullable=False))
    op.add_column('company_profiles', sa.Column('fair_id', mysql.INTEGER(display_width=11), autoincrement=False,
                  nullable=False))
    op.add_column('company_profiles', sa.Column('company_id', mysql.INTEGER(display_width=11), autoincrement=False,
                  nullable=False))
    op.create_foreign_key('company_profiles_ibfk_2', 'company_profiles', 'fairs', ['fair_id'], ['id'])
    op.create_foreign_key('company_profiles_ibfk_1', 'company_profiles', 'companies', ['company_id'], ['id'])
    op.add_column('companies', sa.Column('is_career_center', mysql.TINYINT(display_width=1), autoincrement=False,
                  nullable=False))
    op.add_column('companies', sa.Column('is_cv_check_company', mysql.TINYINT(display_width=1), autoincrement=False,
                  nullable=False))

    # WARNING! Data migration is not revertible!

    op.drop_column('participations', 'updated_time')
    op.drop_column('participations', 'created_time')
    op.drop_constraint(None, 'fairs', type_='foreignkey')
    op.drop_column('fairs', 'registration_start')
    op.drop_column('fairs', 'registration_end')
    op.drop_column('fairs', 'date_two')
    op.drop_column('fairs', 'date_one')
    op.drop_column('fairs', 'cv_check_company_id')
    op.drop_constraint(None, 'company_profiles', type_='foreignkey')
    op.drop_column('company_profiles', 'updated_time')
    op.drop_column('company_profiles', 'participation_id')
    op.drop_column('company_profiles', 'created_time')
    op.drop_column('companies', 'updated_time')
    op.drop_column('companies', 'created_time')
    op.drop_column('booths_booked', 'price')
