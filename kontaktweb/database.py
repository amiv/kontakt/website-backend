from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from marshmallow import validate

db = SQLAlchemy()
ma = Marshmallow()

# Custom Marshmallow validator
not_blank = validate.Length(min=1, error='Field cannot be blank')
