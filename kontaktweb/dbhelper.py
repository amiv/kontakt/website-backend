import hashlib

from .models import (
    Fair, BoothOption, FairDays, BoothSize
)


class DbHelperError(RuntimeError):
    pass


def get_timeslots():
    """
    Return list of dicts containing available space information for each timeslot
    (i.e. a combination of day, booth size and category).

    Assign a unique ID to each timeslot. E.g.:

    [
        {"available_spaces":9,"category":"A","day":"FIRST","id":"8c609117ec79ae33096638b4734753f4","price":2400.0,"size":"LARGE"},
        {"available_spaces":18,"category":"A","day":"FIRST","id":"41909252a72e43544f35be978f3b4b16","price":1150.0,"size":"SMALL"},
        {"available_spaces":7,"category":"B","day":"FIRST","id":"6aeac78e1cb0bc5e8757c34334204f03","price":1800.0,"size":"LARGE"},
        {"available_spaces":15,"category":"B","day":"FIRST","id":"d7c51b641254af7ae3b9d49a3e1791e1","price":850.0,"size":"SMALL"},
        {"available_spaces":3,"category":"C","day":"FIRST","id":"fbcf991d7d063d565dcc091ad5e0a75b","price":550.0,"size":"LARGE"},
        {"available_spaces":7,"category":"C","day":"FIRST","id":"e8a59a1a6a3132ab0f512673ec4f0ea7","price":550.0,"size":"SMALL"},
        {"available_spaces":9,"category":"A","day":"SECOND","id":"b15507ae37c62e2f1e3c1afd4e885c03","price":2400.0,"size":"LARGE"},
        {"available_spaces":18,"category":"A","day":"SECOND","id":"c370f855a71585a1ccce57b3cd2670e2","price":1150.0,"size":"SMALL"},
        {"available_spaces":7,"category":"B","day":"SECOND","id":"642493a8fd965c03139ae41198195337","price":1800.0,"size":"LARGE"},
        {"available_spaces":15,"category":"B","day":"SECOND","id":"e2d72ef5be9f810126254e26283638b7","price":850.0,"size":"SMALL"},
        {"available_spaces":3,"category":"C","day":"SECOND","id":"7089bda8b88ceb3a6f2405b667692075","price":550.0,"size":"LARGE"},
        {"available_spaces":7,"category":"C","day":"SECOND","id":"013899394a79c71f2f81a28179f58691","price":550.0,"size":"SMALL"},
        {"available_spaces":9,"category":"A","day":"BOTH","id":"afa376290b371e290485b2a040525cfb","price":5000.0,"size":"LARGE"},
        {"available_spaces":18,"category":"A","day":"BOTH","id":"495ef27142ee69394e15804548f279ac","price":2500.0,"size":"SMALL"},
        {"available_spaces":7,"category":"B","day":"BOTH","id":"76b132c9ef3019ea8e2c79642e63e059","price":3800.0,"size":"LARGE"},
        {"available_spaces":15,"category":"B","day":"BOTH","id":"0659e519e1fde1b1fc21ecf8b9a4c7e3","price":1900.0,"size":"SMALL"},
        {"available_spaces":3,"category":"C","day":"BOTH","id":"2e61ea48089eb6db976e98bead25f62e","price":0.0,"size":"LARGE"},
        {"available_spaces":7,"category":"C","day":"BOTH","id":"ac4628fcf74c03bedfd3b1bd4176aa80","price":0.0,"size":"SMALL"},
    ]
    """
    spaces = BoothOption.get_avail_spaces()
    h = hashlib.blake2b(digest_size=16)

    space_list = []
    for day in [FairDays.FIRST, FairDays.SECOND, FairDays.BOTH, FairDays.THIRD, FairDays.SECOND_AND_THIRD]:
        for category in spaces[day.name]:
            fair_id = Fair.get_current_fair().id
            booth = BoothOption.query.filter_by(booth_category=category, fair_id=fair_id).first()
            for size in [BoothSize.LARGE, BoothSize.SMALL]:
                h.update(bytes(category + size.name + day.name, 'utf-8'))

                # large booths occupy two spots, small booths only one
                if size == BoothSize.SMALL:
                    available_spaces = spaces[day.name][category]
                else:
                    available_spaces = spaces[day.name][category] // 2

                booth_price = booth.get_booth_price(day, size)

                if booth_price is not None:
                    space_list.append({
                        "category": category,
                        "size": size.name,
                        "day": day.name,
                        "id": h.hexdigest(),
                        "price": booth_price,
                        "available_spaces": available_spaces,
                    })
    return space_list


def get_timeslot_if_available(timeslot_id):
    """
    Check if the requested time slot still has available spaces.
    :param timeslot_id: the ID of the timeslot
    :return: the requested timeslot         if there are still available spaces
             False                          if there are no available spaces
    :raise DbHelperError                    if the timeslot was not found
    """
    for s in get_timeslots():
        if s['id'] == timeslot_id:
            return s if s['available_spaces'] > 0 else False
    raise DbHelperError(f'Unknown timeslot ID: {timeslot_id}')
