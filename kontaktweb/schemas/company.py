from marshmallow import EXCLUDE
from marshmallow_sqlalchemy import auto_field

from ..database import ma
from ..models import Company


class CompanySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Company
        load_instance = True
        include_fk = True
        unknown = EXCLUDE

    id = auto_field(dump_only=True)
    startup = auto_field(dump_only=True)
    company_name = auto_field(dump_only=True)
    created_time = auto_field(dump_only=True)
    updated_time = auto_field(dump_only=True)


# Provides a single instance of the marshmallow schema.
company_schema = CompanySchema()
