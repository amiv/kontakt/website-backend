from marshmallow import fields

from ..database import ma


class FairDayCompaniesSchema(ma.Schema):
    logo = fields.Str()
    booth = fields.Str()
    company_name = fields.Str()
    profile_id = fields.Int()
    website = fields.Str()


class FairCompaniesSchema(ma.Schema):
    FIRST = fields.Nested(FairDayCompaniesSchema, many=True)
    SECOND = fields.Nested(FairDayCompaniesSchema, many=True)
    # TODO what is this?


# Provides singleton instances of the marshmallow schemas.
fair_companies_schema = FairCompaniesSchema()
