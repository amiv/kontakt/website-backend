from marshmallow import EXCLUDE
from marshmallow_sqlalchemy import auto_field

from ..database import ma
from ..models import User


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        load_instance = True
        include_fk = True
        exclude = ['password_hash']
        unknown = EXCLUDE

    id = auto_field(dump_only=True)  # users may not change their ID
    company_id = auto_field(dump_only=True)  # users may not change their company ID


# Provides a single instance of the marshmallow schema.
user_schema = UserSchema()
