from marshmallow.fields import String, Integer, Decimal

from ..database import ma


class TimeslotSchema(ma.Schema):
    id = String()
    available_spaces = Integer()
    category = String()
    day = String()
    price = Decimal(places=2)
    size = String()


# Provides a single instance of the marshmallow schema.
timeslot_schema = TimeslotSchema()
