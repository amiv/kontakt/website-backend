from marshmallow import EXCLUDE

from ..database import ma
from ..models import Fair


class FairSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Fair
        load_instance = True
        include_fk = True
        unknown = EXCLUDE


# Provides singleton instances of the marshmallow schemas.
fair_schema = FairSchema()
