import json

from marshmallow import ValidationError, EXCLUDE
from marshmallow.fields import Method
from marshmallow_sqlalchemy import auto_field

from ..database import ma
from ..models.company_profile import CompanyProfile


class CompanyProfileSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CompanyProfile
        load_instance = True
        include_fk = True
        unknown = EXCLUDE

    @staticmethod
    def _serialize_logo(company_profile):
        return f'/company_profiles/{company_profile.id}/logo' if company_profile.logo else None

    @staticmethod
    def _serialize_advertisement(company_profile):
        return f'/company_profiles/{company_profile.id}/advertisement' if company_profile.advertisement else None

    @staticmethod
    def _serialize_interesting_students(company_profile):
        try:
            return json.loads(company_profile.interesting_students)
        except json.JSONDecodeError as e:
            raise ValidationError(str(e))

    @staticmethod
    def _serialize_offers(company_profile):
        try:
            return json.loads(company_profile.offers)
        except json.JSONDecodeError as e:
            raise ValidationError(str(e))

    @staticmethod
    def _deserialize_as_is(value):
        return value

    id = auto_field(dump_only=True)
    participation_id = auto_field(dump_only=True)
    created_time = auto_field(dump_only=True)
    updated_time = auto_field(dump_only=True)
    logo = Method('_serialize_logo')
    advertisement = Method('_serialize_advertisement')
    interesting_students = Method('_serialize_interesting_students', '_deserialize_as_is')
    offers = Method('_serialize_offers', '_deserialize_as_is')


# Provides a single instance of the marshmallow schema.
company_profile_schema = CompanyProfileSchema()
