from marshmallow import EXCLUDE
from marshmallow.fields import Method, Nested

from ..database import ma
from ..models import BoothBooked
from .booth_option import BoothOptionSchema


class BoothBookedSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = BoothBooked
        load_instance = True
        include_fk = True
        unknown = EXCLUDE

    @staticmethod
    def _serialize_day(boothBooked):
        day = boothBooked.day
        if day is None:
            return None
        return day.name

    @staticmethod
    def _serialize_booth_size(boothBooked):
        booth_size = boothBooked.booth_size
        if booth_size is None:
            return None
        return booth_size.name

    @staticmethod
    def _serialize_table_type(boothBooked):
        table_type = boothBooked.table_type
        if table_type is None:
            return None
        return table_type.name

    day = Method('_serialize_day')
    booth_size = Method('_serialize_booth_size')
    table_type = Method('_serialize_table_type')


class BoothBookedEmbeddedOptionSchema(BoothBookedSchema):
    booth_option = Nested(BoothOptionSchema, dump_only=True)


# Provides a single instance of the marshmallow schema.
booth_booked_schema = BoothBookedSchema()
