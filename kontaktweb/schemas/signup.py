from marshmallow import fields, ValidationError
from marshmallow.fields import Method

from ..database import ma
from ..models.enums import TableType


class SignupSchema(ma.Schema):
    class Meta:
        load_only = [
            'timeslot', 'packages', 'table_type', 'message',
            'accepted_participation_terms', 'accepted_permitted_items_terms'
        ]

    @staticmethod
    def _deserialize_table_type(value):
        try:
            return TableType(int(value))
        except ValueError:
            raise ValidationError(f'Invalid value for field "table_type": {value}. '
                                  f'Allowed values are: 0 (one single low table), '
                                  f'1 (two narrow tall tables)')

    timeslot = fields.Str()
    packages = fields.List(fields.Int())
    table_type = Method(deserialize='_deserialize_table_type')
    message = fields.Str()
    accepted_participation_terms = fields.Bool()
    accepted_permitted_items_terms = fields.Bool()


signup_schema = SignupSchema()
