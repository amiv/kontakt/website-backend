from marshmallow import EXCLUDE

from ..database import ma
from ..models import BoothOption


class BoothOptionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = BoothOption
        load_instance = True
        include_fk = True
        unknown = EXCLUDE


# Provides a single instance of the marshmallow schema.
booth_option_schema = BoothOptionSchema()
