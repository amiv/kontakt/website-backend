from marshmallow import EXCLUDE
from marshmallow_sqlalchemy import fields

from ..database import ma
from ..models import PackageBooked
from .package_option import PackageOptionSchema


class PackageBookedSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = PackageBooked
        load_instance = True
        include_fk = True
        unknown = EXCLUDE


class PackageBookedEmbeddedOptionSchema(PackageBookedSchema):
    package_option = fields.Nested(PackageOptionSchema, dump_only=True)


# Provides a single instance of the marshmallow schema.
package_booked_schema = PackageBookedSchema()
