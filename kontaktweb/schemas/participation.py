from marshmallow import EXCLUDE
from marshmallow.fields import Method, Nested
from marshmallow_sqlalchemy import auto_field

from ..database import ma
from ..models import Participation

from .package_booked import PackageBookedEmbeddedOptionSchema
from .booth_booked import BoothBookedEmbeddedOptionSchema
from .fair import FairSchema


class ParticipationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Participation
        load_instance = True
        include_fk = True
        unknown = EXCLUDE

    id = auto_field(dump_only=True)
    company_id = auto_field(dump_only=True)
    fair_id = auto_field(dump_only=True)
    created_time = auto_field(dump_only=True)
    updated_time = auto_field(dump_only=True)
    time_of_signup = auto_field(dump_only=True)
    packages_booked = Nested(PackageBookedEmbeddedOptionSchema, many=True, dump_only=True)
    booths_booked = Nested(BoothBookedEmbeddedOptionSchema, many=True, dump_only=True)
    total_price = Method('get_total_price', dump_only=True)

    def get_total_price(self, obj):
        return obj.get_total_price()


class ParticipationFairEmbeddedSchema(ParticipationSchema):
    fair = Nested(FairSchema, dump_only=True)


# Provides singleton instances of the marshmallow schemas.
participation_schema = ParticipationSchema()
participation_fair_embedded_schema = ParticipationFairEmbeddedSchema()
