from marshmallow import EXCLUDE

from ..database import ma
from ..models import PackageOption


class PackageOptionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = PackageOption
        load_instance = True
        include_fk = True
        exclude = ['description_long_de', 'description_long_en']
        unknown = EXCLUDE


# Provides a single instance of the marshmallow schema.
package_option_schema = PackageOptionSchema()
