from marshmallow import fields
from marshmallow.validate import Length

from ...database import ma, not_blank


class PasswordSetSchema(ma.Schema):
    class Meta:
        load_only = ['email', 'token', 'new_password']

    email = fields.Str(required=True, validate=not_blank)
    token = fields.Str(required=True, validate=not_blank)
    new_password = fields.Str(required=True, validate=Length(min=8))
