from marshmallow import fields
from marshmallow.validate import Length

from ...database import ma, not_blank


class PasswordChangeSchema(ma.Schema):
    class Meta:
        load_only = ['old_password', 'new_password']

    old_password = fields.Str(required=True, validate=not_blank)
    new_password = fields.Str(required=True, validate=Length(min=8))
