from marshmallow import fields

from ...database import ma, not_blank


class CheckTokenSchema(ma.Schema):
    class Meta:
        load_only = ['email', 'token']

    email = fields.Str(required=True, validate=not_blank)
    token = fields.Str(required=True, validate=not_blank)
