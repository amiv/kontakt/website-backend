from .login import LoginSchema
from .check_token import CheckTokenSchema
from .password_set import PasswordSetSchema
from .password_change import PasswordChangeSchema
from .request_password_reset import RequestPasswordResetSchema

# Prepared Schema instances
check_token_schema = CheckTokenSchema()
login_schema = LoginSchema()
password_change_schema = PasswordChangeSchema()
password_set_schema = PasswordSetSchema()
request_password_reset_schema = RequestPasswordResetSchema()
