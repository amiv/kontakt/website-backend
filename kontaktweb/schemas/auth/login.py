from marshmallow import fields

from ...database import ma, not_blank


class LoginSchema(ma.Schema):
    class Meta:
        load_only = ['email', 'password']

    email = fields.Str(required=True, validate=not_blank)
    password = fields.Str(required=True, validate=not_blank)
