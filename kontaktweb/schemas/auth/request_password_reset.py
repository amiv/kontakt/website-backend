from marshmallow import fields

from ...database import ma


class RequestPasswordResetSchema(ma.Schema):
    class Meta:
        load_only = ['email']

    email = fields.Email(required=True)
