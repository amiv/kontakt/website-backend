import functools

from flask import jsonify
from werkzeug.wrappers import BaseResponse
from werkzeug.datastructures import Headers
from webargs.flaskparser import use_args, use_kwargs


def response(schema, code=200, **kwargs):
    """
    Decorate a view function to return an object serialized with the given schema.
    :param view: The view function to be wrapped
    :return: The wrapped view function
    """
    def decorator(func):
        @functools.wraps(func)
        def wrapped(*f_args, **f_kwargs):
            # Execute decorated function
            result_raw, status, headers = unpack_tuple_response(
                func(*f_args, **f_kwargs))

            # If return value is a werkzeug BaseResponse, return it
            if isinstance(result_raw, BaseResponse):
                return result_raw

            # Dump result with schema if specified
            if schema is None:
                result_dump = result_raw
            else:
                result_dump = jsonify(schema.dump(result_raw, **kwargs))
            return result_dump

        return wrapped
    return decorator


def arguments(schema, location='json', unknown='EXCLUDE', **kwargs):
    """
    Decorate a view function to parse arguments with the given schema.
    :param view: The view function to be wrapped
    :return: The wrapped view function
    """
    def decorator(func):
        @functools.wraps(func)
        def wrapped(*f_args, **f_kwargs):
            return func(*f_args, **f_kwargs)

        # Call use_args (from webargs) to inject params in function
        return use_args(schema, location=location, unknown=unknown, **kwargs)(wrapped)
    return decorator


def kw_arguments(schema, location='json', unknown='EXCLUDE', **kwargs):
    """
    Decorate a view function to parse arguments with the given schema.
    :param view: The view function to be wrapped
    :return: The wrapped view function
    """
    def decorator(func):
        @functools.wraps(func)
        def wrapped(*f_args, **f_kwargs):
            return func(*f_args, **f_kwargs)

        # Call use_kwargs (from webargs) to inject params in function
        return use_kwargs(schema, location=location, unknown=unknown, **kwargs)(wrapped)
    return decorator


# Copied from Flask
def unpack_tuple_response(rv):
    """Unpack a flask Response tuple"""

    status = headers = None

    # unpack tuple returns
    # Unlike Flask, we check exact type because tuple subclasses may be
    # returned by view functions
    if type(rv) is tuple:  # pylint: disable=unidiomatic-typecheck
        len_rv = len(rv)

        # a 3-tuple is unpacked directly
        if len_rv == 3:
            rv, status, headers = rv
        # decide if a 2-tuple has status or headers
        elif len_rv == 2:
            if isinstance(rv[1], (Headers, dict, tuple, list)):
                rv, headers = rv
            else:
                rv, status = rv
        # other sized tuples are not allowed
        else:
            raise TypeError(
                'The view function did not return a valid response tuple.'
                ' The tuple must have the form (body, status, headers),'
                ' (body, status), or (body, headers).'
            )

    return rv, status, headers
