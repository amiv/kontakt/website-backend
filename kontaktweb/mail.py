from flask_mail import Mail, Message
from flask import render_template, current_app as app
import urllib

mail = Mail()


def send_password_reset_mail(user, token):
    contact_email = app.config.get('CONTACT_EMAIL', 'kontaktmesse@amiv.ethz.ch')
    password_reset_link_de = (
        app.config.get('WEBSITE_URL') +
        '/de/password-reset?email=' + urllib.parse.quote(user.email_address) +
        '&token=' + token
    )
    password_reset_link_en = (
        app.config.get('WEBSITE_URL') +
        '/en/password-reset?email=' + urllib.parse.quote(user.email_address) +
        '&token=' + token
    )

    name = user.first_name + ' ' + user.last_name
    msg = Message(subject='Reset Password',
                  body=render_template('password_reset.txt', name=name, password_reset_link_de=password_reset_link_de,
                                       password_reset_link_en=password_reset_link_en, contact_email=contact_email),
                  html=render_template('password_reset.html', name=name, password_reset_link_de=password_reset_link_de,
                                       password_reset_link_en=password_reset_link_en, contact_email=contact_email),
                  recipients=[user.email_address],
                  reply_to=contact_email)
    mail.send(msg)
