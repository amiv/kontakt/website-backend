from sqlalchemy import Integer, String, Enum, UnicodeText, Numeric

from .enums import FairDays, BoothSize, TableType
from ..database import db


class BoothBooked(db.Model):
    """
    A booked booth for a participation.
    """

    __tablename__ = 'booths_booked'

    id = db.Column(Integer, primary_key=True)
    participation_id = db.Column(Integer, db.ForeignKey('participations.id'), nullable=False)
    booth_id = db.Column(Integer, db.ForeignKey('booth_options.id'), nullable=False)
    day = db.Column(Enum(FairDays), nullable=False)
    booth_size = db.Column(Enum(BoothSize), nullable=False)
    table_type = db.Column(Enum(TableType), nullable=False)
    message = db.Column(UnicodeText)
    booth_location = db.Column(String(100), nullable=True)
    price = db.Column(Numeric(10, 2), nullable=False)

    participation = db.relationship('Participation', back_populates='booths_booked')
    booth_option = db.relationship('BoothOption', back_populates='booths_booked')

    @staticmethod
    def persist(booth_booked):
        db.session.add(booth_booked)
        db.session.commit()
