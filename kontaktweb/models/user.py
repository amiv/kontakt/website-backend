from sqlalchemy import Integer, String
from flask import current_app
from passlib.hash import pbkdf2_sha256
from datetime import datetime
import secrets

from .usertoken import UserToken
from ..database import db


class User(db.Model):
    """
    User object.
    """

    __tablename__ = 'users'

    id = db.Column(Integer, primary_key=True)
    company_id = db.Column(Integer, db.ForeignKey('companies.id'), nullable=False)
    first_name = db.Column(String(100), nullable=False)
    last_name = db.Column(String(100), nullable=False)
    email_address = db.Column(String(100), nullable=False)
    password_hash = db.Column(String(200), nullable=False)
    phone = db.Column(String(100))

    company = db.relationship('Company', back_populates='users')
    tokens = db.relationship('UserToken', back_populates='user', lazy='dynamic')

    @staticmethod
    def login(email, password):
        """Logs in user with email and password, if successful returns the user object otherwise returns None."""
        user = User.query.filter_by(email_address=email).first()
        if user is None:
            return None
        return user if pbkdf2_sha256.verify(password, user.password_hash) else None

    def check_password(self, password):
        """Check password agains stored hash."""
        return pbkdf2_sha256.verify(password, self.password_hash)

    def set_password(self, password):
        """Update the password by computing and saving the new hash."""
        self.password_hash = pbkdf2_sha256.hash(password)
        db.session.commit()

    def is_token_valid(self, token):
        """Check whether the given token is valid."""
        token_item = self.tokens.filter_by(token=token).first()
        return token_item is not None and token_item.is_valid()

    def delete_token(self, token):
        """Delete/invalidate the given token."""
        self.tokens.filter_by(token=token).delete()
        db.session.commit()

    def create_token(self):
        """Create a new token for the user."""
        token_item = UserToken()
        token_item.user_id = self.id
        token_item.token = secrets.token_urlsafe(16)
        token_item.expire_date = datetime.now() + current_app.config.get('USER_TOKEN_TIMEOUT')

        db.session.add(token_item)
        db.session.commit()

        return token_item

    @staticmethod
    def persist(user):
        db.session.add(user)
        db.session.commit()

    @staticmethod
    def get_by_email(email):
        """Get User object by email address."""
        return User.query.filter_by(email_address=email).first()

    @classmethod
    def exists(cls, email):
        """Check whether a user with the given email address exists."""
        return cls.get_by_email(email) is not None
