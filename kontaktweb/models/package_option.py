from sqlalchemy import Integer, String, Numeric, UnicodeText, Boolean

from ..database import db
from .fair import Fair


class PackageOption(db.Model):
    """
    Package options for fair participation.
    """

    __tablename__ = 'package_options'

    id = db.Column(Integer, primary_key=True)
    fair_id = db.Column(Integer, db.ForeignKey('fairs.id'), nullable=False)
    mutex_package_id = db.Column(Integer, db.ForeignKey('package_options.id'), nullable=True)
    title_de = db.Column(String(100), nullable=False)
    title_en = db.Column(String(100), nullable=False)
    description_de = db.Column(UnicodeText, nullable=False)
    description_en = db.Column(UnicodeText, nullable=False)
    description_long_de = db.Column(UnicodeText, nullable=True)
    description_long_en = db.Column(UnicodeText, nullable=True)
    price = db.Column(Numeric(10, 2), nullable=False)
    includes_ad = db.Column(Boolean, nullable=False, default=False)
    multiple = db.Column(Boolean, nullable=False, default=False)
    price_multiple = db.Column(Numeric(10, 2), nullable=True, default=True)
    fair = db.relationship('Fair', back_populates='package_options')
    packages_booked = db.relationship('PackageBooked', back_populates='package_option')

    @staticmethod
    def get_for_current_fair():
        fair_id = Fair.get_current_fair().id
        return PackageOption.query.filter_by(fair_id=fair_id).all()

    @staticmethod
    def persist(package_option):
        db.session.add(package_option)
        db.session.commit()
