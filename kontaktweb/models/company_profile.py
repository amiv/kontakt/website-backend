from sqlalchemy import Integer, String, LargeBinary, UnicodeText, DateTime
from datetime import datetime

from .fair import Fair
from ..database import db


class CompanyProfile(db.Model):
    """
    Company Profile model
    """

    __tablename__ = 'company_profiles'

    id = db.Column(Integer, primary_key=True)
    participation_id = db.Column(Integer, db.ForeignKey('participations.id'), nullable=False)
    website = db.Column(String(100), nullable=True)
    about_us = db.Column(UnicodeText, nullable=True)
    focus = db.Column(UnicodeText, nullable=True)
    offers = db.Column(String(100), nullable=True)
    interesting_students = db.Column(String(100), nullable=True)
    contact_for_students = db.Column(UnicodeText, nullable=True)
    employees_world = db.Column(Integer, nullable=True)
    employees_ch = db.Column(Integer, nullable=True)
    locations = db.Column(UnicodeText, nullable=True)
    logo = db.Column(LargeBinary, nullable=True)
    advertisement = db.Column(LargeBinary, nullable=True)

    # Intentionally no default specified in the schema. DateTime should only be filled out
    # by application and not by the database server itself.
    created_time = db.Column(DateTime, nullable=False)
    updated_time = db.Column(DateTime, nullable=False)

    participation = db.relationship('Participation', back_populates='company_profile')

    @staticmethod
    def get_profile(company_id, fair_id=None):
        """Returns company profile of the company.
        If the company id is equal to the string 'all', all profiles of the participating companies are returned.
        If 'fair_id' is left empty, the current fair id will be used.
        """
        if fair_id is None:
            fair_id = Fair.get_current_fair().id
        if company_id == "all":
            return CompanyProfile.query.filter_by(fair_id=fair_id).all()
        return CompanyProfile.query.filter_by(fair_id=fair_id, company_id=company_id).first()

    @staticmethod
    def persist(company_profile):
        """
        :param company_profile: CompanyProfile instance to persist
        :return: None
        """
        now = datetime.now()

        if company_profile.created_time is None:
            company_profile.created_time = now
        company_profile.updated_time = now

        db.session.add(company_profile)
        db.session.commit()
