from sqlalchemy import Integer, String, Boolean, DateTime
from datetime import datetime
from ..database import db


class Company(db.Model):
    """
    General Company information
    """

    __tablename__ = 'companies'

    id = db.Column(Integer, primary_key=True)
    company_name = db.Column(String(100), nullable=False)
    billing_address_street = db.Column(String(100), nullable=True)
    billing_address_postal_code = db.Column(String(100), nullable=True)
    billing_address_city = db.Column(String(100), nullable=True)
    billing_address_country = db.Column(String(100), nullable=True)
    billing_recipient = db.Column(String(100), nullable=True)
    billing_comment = db.Column(String(400), nullable=True)
    correspondence_email_address = db.Column(String(100))
    secondary_email_address = db.Column(String(100))
    billing_email_address = db.Column(String(100))
    startup = db.Column(Boolean, nullable=False, default=False)

    # Intentionally no default specified in the schema. DateTime should only be filled out
    # by application and not by the database server itself.
    created_time = db.Column(DateTime, nullable=False)
    updated_time = db.Column(DateTime, nullable=False)

    users = db.relationship('User', back_populates='company')
    participations = db.relationship('Participation', back_populates='company')
    cv_check_fairs = db.relationship('Fair', back_populates='cv_check_company')

    @staticmethod
    def persist(company):
        now = datetime.now()

        if company.created_time is None:
            company.created_time = now
        company.updated_time = now

        db.session.add(company)
        db.session.commit()
