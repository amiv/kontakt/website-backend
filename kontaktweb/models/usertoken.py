from sqlalchemy import Integer
from datetime import datetime, timedelta

from ..database import db


class UserToken(db.Model):
    """
    UserToken object.
    """

    __tablename__ = 'usertokens'

    id = db.Column(Integer, primary_key=True)
    user_id = db.Column(Integer, db.ForeignKey('users.id'), nullable=False)
    token = db.Column(db.String(32), nullable=False, index=True)
    expire_date = db.Column(db.DateTime, nullable=False)

    user = db.relationship('User', back_populates='tokens')

    def is_valid(self):
        return self.expire_date - datetime.now() > timedelta(seconds=0)

    @staticmethod
    def persist(usertoken):
        db.session.add(usertoken)
        db.session.commit()
