from sqlalchemy import Integer, String, Boolean, Numeric, Date, DateTime

from ..database import db


class Fair(db.Model):
    """
    Fair configuration entry.
    """

    __tablename__ = 'fairs'

    id = db.Column(Integer, primary_key=True)
    kontakt_title = db.Column(String(100), nullable=False)
    kontakt_president = db.Column(String(100), nullable=False)
    kontakt_treasurer = db.Column(String(100), nullable=False)
    cv_check_company_id = db.Column(Integer, db.ForeignKey('companies.id'), nullable=True)
    is_current = db.Column(Boolean, nullable=False)
    date_start = db.Column(Date, nullable=False)
    date_end = db.Column(Date, nullable=False)
    number_of_days = db.Column(Integer, nullable=False, default=2)
    registration_start = db.Column(DateTime, nullable=False)
    registration_end = db.Column(DateTime, nullable=False)
    profile_deadline = db.Column(Date, nullable=False)
    fairguide_changes_deadline = db.Column(Date, nullable=False)
    startup_discount = db.Column(Numeric(10, 2), nullable=False)

    cv_check_company = db.relationship('Company', back_populates='cv_check_fairs')
    participations = db.relationship('Participation', back_populates='fair')
    booth_options = db.relationship('BoothOption', back_populates='fair')
    package_options = db.relationship('PackageOption', back_populates='fair')

    @staticmethod
    def get_current_fair():
        return Fair.query.filter_by(is_current=True).first()

    @staticmethod
    def persist(fair):
        db.session.add(fair)
        db.session.commit()
