from enum import IntEnum


class BoothSize(IntEnum):
    SMALL = 0
    LARGE = 1


class FairDays(IntEnum):
    FIRST = 1
    SECOND = 2
    BOTH = 3
    THIRD = 4
    SECOND_AND_THIRD = 5


class TableType(IntEnum):
    LOW = 0
    HIGH = 1
