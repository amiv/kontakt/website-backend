from sqlalchemy import Integer, String, Numeric

from ..database import db
from .fair import Fair
from .booth_booked import BoothBooked
from .participation import Participation
from .enums import FairDays, BoothSize


class BoothOption(db.Model):
    """
    Booth options for fair participation.
    """

    __tablename__ = 'booth_options'

    id = db.Column(Integer, primary_key=True)
    fair_id = db.Column(Integer, db.ForeignKey('fairs.id'), nullable=False)
    booth_category = db.Column(String(1), nullable=False)
    day_one_spots = db.Column(Integer, nullable=False)
    day_two_spots = db.Column(Integer, nullable=False)
    day_three_spots = db.Column(Integer, nullable=True)
    price_one_day_small = db.Column(Numeric(10, 2), nullable=True)
    price_two_days_small = db.Column(Numeric(10, 2), nullable=True)
    price_one_day_large = db.Column(Numeric(10, 2), nullable=True)
    price_two_days_large = db.Column(Numeric(10, 2), nullable=True)
    price_three_days_small = db.Column(Numeric(10, 2), nullable=True)
    price_three_days_large = db.Column(Numeric(10, 2), nullable=True)

    booths_booked = db.relationship('BoothBooked', back_populates='booth_option')
    fair = db.relationship('Fair', back_populates='booth_options')

    def get_booth_price(self, day, size):
        """Returns the price of the booth with the given day(s) and size (as enums)."""
        if day == FairDays.FIRST or day == FairDays.SECOND or day == FairDays.THIRD:
            if size == BoothSize.SMALL:
                return self.price_one_day_small
            else:
                return self.price_one_day_large
        elif day == FairDays.BOTH or day == FairDays.SECOND_AND_THIRD:
            if size == BoothSize.SMALL:
                return self.price_two_days_small
            else:
                return self.price_two_days_large
        raise ValueError("Day and/or size are no valid parameters")

    @staticmethod
    def get_avail_spaces():
        """Return dict of available spaces in all categories on all days of the current fair.
        The returned dict has the following structure:
        {
            'FIRST': {
                'A': <available_spots>,
                'B': <available_spots>,
                ...
            },
            'SECOND': {
                'A': <available_spots>,
                'B': <available_spots>,
                ...
            },
            'THIRD': {
                'A': <available_spots>,
                'B': <available_spots>,
                ...
            },
            'BOTH': {
                'A': <min(first, second)>,
                'B': <min(first, second)>,
                ...
            },
            'SECOND_AND_THIRD': {
                'A': <min(second, third)>,
                'B': <min(second, third)>,
                ...
            },
        }
        Bookings for combination days subtract from each individual day. For example, a booking on
        FairDays.BOTH subtracts from both 'FIRST' and 'SECOND' while a booking on FairDays.SECOND_AND_THIRD
        subtracts from both 'SECOND' and 'THIRD'.
        """
        fair = Fair.get_current_fair()
        booth_options = BoothOption.query.filter_by(fair_id=fair.id).all()
        booths_booked = (
            BoothBooked.query
            .join(Participation)
            .filter(Participation.fair_id == fair.id)
            .all()
        )
        # Initialize available spaces for each day and combination options.
        avail_spaces = {
            "FIRST": {},
            "SECOND": {},
            "THIRD": {},
            "BOTH": {},
            "SECOND_AND_THIRD": {}
        }
        # Initialize with total number of booths per category.
        for option in booth_options:
            avail_spaces["FIRST"][option.booth_category] = option.day_one_spots
            avail_spaces["SECOND"][option.booth_category] = option.day_two_spots
            avail_spaces["THIRD"][option.booth_category] = option.day_three_spots
            # We'll compute the combined availabilities below.
            avail_spaces["BOTH"][option.booth_category] = 0
            avail_spaces["SECOND_AND_THIRD"][option.booth_category] = 0
        # Subtract available spaces for each booked booth.
        for p in booths_booked:
            category = p.booth_option.booth_category
            if p.booth_size == BoothSize.LARGE:
                # For large booths, subtract 2 spots.
                if p.day in [FairDays.FIRST, FairDays.BOTH]:
                    avail_spaces["FIRST"][category] -= 2
                if p.day in [FairDays.SECOND, FairDays.BOTH, FairDays.SECOND_AND_THIRD]:
                    avail_spaces["SECOND"][category] -= 2
                if p.day in [FairDays.THIRD, FairDays.SECOND_AND_THIRD]:
                    avail_spaces["THIRD"][category] -= 2
            elif p.booth_size == BoothSize.SMALL:
                # For small booths, subtract 1 spot.
                if p.day in [FairDays.FIRST, FairDays.BOTH]:
                    avail_spaces["FIRST"][category] -= 1
                if p.day in [FairDays.SECOND, FairDays.BOTH, FairDays.SECOND_AND_THIRD]:
                    avail_spaces["SECOND"][category] -= 1
                if p.day in [FairDays.THIRD, FairDays.SECOND_AND_THIRD]:
                    avail_spaces["THIRD"][category] -= 1
        # Calculate combined available spaces.
        # 'BOTH' represents the combination of FIRST and SECOND days.
        for cat in avail_spaces["FIRST"]:
            avail_spaces["BOTH"][cat] = min(avail_spaces["FIRST"][cat], avail_spaces["SECOND"][cat])
        # 'SECOND_AND_THIRD' represents the combination of SECOND and THIRD days.
        for cat in avail_spaces["SECOND"]:
            avail_spaces["SECOND_AND_THIRD"][cat] = min(avail_spaces["SECOND"][cat], avail_spaces["THIRD"][cat])
        return avail_spaces

    @staticmethod
    def persist(booth_option):
        """
        :param booth_option: BoothOption instance to persist
        :return: None
        """
        db.session.add(booth_option)
        db.session.commit()
