import json
from datetime import datetime
from sqlalchemy import Integer, UnicodeText, DateTime, Boolean

from .company_profile import CompanyProfile
from ..database import db


class Participation(db.Model):
    """
    Participation entry for a company on a specific fair.
    """

    __tablename__ = 'participations'

    id = db.Column(Integer, primary_key=True)
    company_id = db.Column(Integer, db.ForeignKey('companies.id'), nullable=False)
    fair_id = db.Column(Integer, db.ForeignKey('fairs.id'), nullable=False)

    # Intentionally no default specified in the schema. DateTime should only be filled out
    # by application and not by the database server itself.
    created_time = db.Column(DateTime, nullable=False)
    updated_time = db.Column(DateTime, nullable=False)

    message = db.Column(UnicodeText)
    time_of_signup = db.Column(DateTime, nullable=False, server_default=db.func.now())

    accepted_participation_terms = db.Column(Boolean, nullable=False, default=True)
    accepted_permitted_items_terms = db.Column(Boolean, nullable=False, default=True)

    fair = db.relationship('Fair', back_populates='participations')
    company = db.relationship('Company', back_populates='participations')
    company_profile = db.relationship('CompanyProfile', uselist=False, back_populates='participation')
    packages_booked = db.relationship('PackageBooked', back_populates='participations')
    booths_booked = db.relationship('BoothBooked', back_populates='participation')

    def get_booth_price(self):
        from .booth_booked import BoothBooked

        price = 0
        booths = BoothBooked.query.filter_by(participation_id=self.id).all()
        for booth_booked in booths:
            price += float(booth_booked.price)
        return price

    def get_package_price(self):
        from .package_booked import PackageBooked

        price = 0
        packages = PackageBooked.query.filter_by(participation_id=self.id).all()
        for pack in packages:
            price += float(pack.package_option.price)
        return price

    def get_total_price(self):
        price = self.get_booth_price()
        price += self.get_package_price()

        # subtract startup discount (if applicable)
        if self.company.startup:
            price -= float(self.fair.startup_discount)

        return price

    def create_company_profile(self):
        """Creates a new company profile for this participation and copies the profile of the last fair if it exists."""
        # copy profile from last year's fair, if it exists
        new_profile = None
        last_profile = None
        participations = Participation.query.filter_by(company_id=self.company_id).order_by(
            Participation.updated_time.desc())

        for participation in participations:
            if last_profile is None and participation.company_profile is not None:
                last_profile = participation.company_profile

        if last_profile is not None:
            new_profile = CompanyProfile(website=last_profile.website,
                                         about_us=last_profile.about_us,
                                         focus=last_profile.focus,
                                         offers=last_profile.offers,
                                         interesting_students=last_profile.interesting_students,
                                         contact_for_students=last_profile.contact_for_students,
                                         employees_world=last_profile.employees_world,
                                         employees_ch=last_profile.employees_ch,
                                         locations=last_profile.locations,
                                         logo=last_profile.logo)

        # otherwise, create and initialize a new profile
        if not new_profile:
            new_profile = CompanyProfile()
            new_profile.interesting_students = json.dumps({'itet': False, 'mavt': False, 'mtec': False})
            new_profile.offers = json.dumps({'bachelor': False, 'internship': False, 'semester': False,
                                             'master': False, 'fulltime': False})

        now = datetime.now()

        if new_profile.created_time is None:
            new_profile.created_time = now
        new_profile.updated_time = now

        self.company_profile = new_profile
        db.session.add(new_profile)
        db.session.commit()

        return new_profile

    @staticmethod
    def persist(participation):
        now = datetime.now()

        if participation.created_time is None:
            participation.created_time = now
        participation.updated_time = now

        db.session.add(participation)
        db.session.commit()
