from sqlalchemy import Integer

from .fair import Fair
from .participation import Participation
from ..database import db


class PackageBooked(db.Model):
    """
    Booked packages for fair participation.
    """

    __tablename__ = 'packages_booked'

    package_id = db.Column(Integer, db.ForeignKey('package_options.id'), primary_key=True)
    participation_id = db.Column(Integer, db.ForeignKey('participations.id'), primary_key=True)
    count = db.Column(Integer, nullable=False, default=1)
    package_option = db.relationship('PackageOption', back_populates='packages_booked')
    participations = db.relationship('Participation', back_populates='packages_booked')

    # todo: wrong now, correct
    @staticmethod
    def get_booked_packages(c_id):
        """Get list of booked packages for given company for the current fair.
        :param c_id the company ID
        :return list of booked packages for the company for the current year
        """
        f_id = Fair.get_current_fair().id
        participation = Participation.query.filter_by(company_id=c_id, fair_id=f_id).first()
        if not participation:
            return []
        return participation.packages_booked

    @staticmethod
    def has_ad(company_id):
        """Return whether or not the company's current booking includes an ad.
        :return bool
        """
        packs = PackageBooked.get_booked_packages(company_id)
        for package in packs:
            if package.package_option.includes_ad:
                return True
        return False
