from flask import abort


def bad_request(description=''):
    abort(400, description=description)


def unauthorized(description=''):
    abort(401, description=description)


def forbidden(description=''):
    abort(403, description=description)


def not_found(description=''):
    abort(404, description=description)


def unprocessable_request(description=''):
    abort(422, description=description)


def conflict(description=''):
    abort(409, description=description)


def internal_error(description='An unknown error has occurred.'):
    abort(500, description=description)
