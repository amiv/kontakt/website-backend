from flask import Blueprint, g, request
from marshmallow import ValidationError

from ..models import Company
from ..schemas import company_schema
from .auth import login_required
from ..errorhandlers import not_found, unprocessable_request, forbidden
from ..apihelper import response


bp = Blueprint('companies', __name__, url_prefix='/companies')


@bp.route('/<c_id>', methods=['PATCH'])
@response(company_schema, code=200)
@login_required
def patch_company(c_id):
    """Update an existing company."""
    company = Company.query.get(c_id)
    if not company:
        return not_found()

    if company.id != g.user.company_id:
        return forbidden()

    try:
        company = company_schema.load(request.json, instance=company, unknown='EXCLUDE')

        print(company)

        Company.persist(company)
    except ValidationError as e:
        return unprocessable_request(e)

    return company


@bp.route('/<c_id>', methods=['GET'])
@response(company_schema, code=200)
@login_required
def get_company(c_id):
    """Returns the requested company object"""
    company = Company.query.get(c_id)

    if not company:
        return not_found()

    if company.id != g.user.company_id:
        return forbidden()

    return company
