from flask import Blueprint, g, request
from marshmallow import ValidationError

from ..apihelper import response
from ..schemas import user_schema
from ..models import User
from ..errorhandlers import unprocessable_request, not_found, forbidden
from .auth import login_required


bp = Blueprint('users', __name__, url_prefix='/users')


@bp.route('/<u_id>', methods=['GET'])
@response(user_schema, code=200)
@login_required
def get_user(u_id):
    """Get user information as JSON
    :return: 200 OK              on success
             404 Not Found       if user with u_id does not exist
    """
    if g.user.id == u_id:
        return g.user
    return not_found()


@bp.route('/<u_id>', methods=['PATCH'])
@response(user_schema, code=200)
@login_required
def patch_user(u_id):
    if g.user.id != int(u_id):
        return forbidden()

    try:
        g.user = user_schema.load(request.json, instance=g.user, unknown='EXCLUDE')
        User.persist(g.user)
    except ValidationError as e:
        return unprocessable_request(e)

    return g.user
