import pytz
from datetime import datetime
from marshmallow import ValidationError
from flask import Blueprint, g, current_app, request

from .auth import login_required
from ..schemas import signup_schema
from ..schemas.participation import participation_schema, participation_fair_embedded_schema
from ..models import Fair, Participation, BoothOption, BoothBooked, PackageOption, PackageBooked, FairDays, BoothSize
from ..database import db
from ..dbhelper import get_timeslot_if_available
from ..errorhandlers import not_found, forbidden, conflict, unprocessable_request
from ..apihelper import response, kw_arguments


bp = Blueprint('participations', __name__, url_prefix='/participations')


@bp.route('/current', methods=['GET'])
@response(participation_schema, code=200)
@login_required
def get_participation_for_current_user_and_fair():
    """Returns the participation object for the authenticated user's company for the current fair.
    """
    participation = Participation.query.filter_by(company_id=g.user.company_id,
                                                  fair_id=Fair.get_current_fair().id).first()
    if participation is None:
        return not_found('You are not yet signed up for this year\'s fair.')
    return participation


@bp.route('/past', methods=['GET'])
@response(participation_fair_embedded_schema, code=200, many=True)
@login_required
def get_particiations():
    """Returns the participation objects.
    :return: 200 OK
    """
    return Participation.query.filter_by(company_id=g.user.company_id).filter(
        Participation.fair_id != Fair.get_current_fair().id).all()  # noqa: E712


@bp.route('/signup', methods=['POST'])
@kw_arguments(signup_schema)
@response(participation_schema, code=201)
@login_required
def post_participation(timeslot, packages, table_type, message, accepted_participation_terms,
                       accepted_permitted_items_terms):
    """Create a participation object"""
    fair = Fair.get_current_fair()
    company = g.user.company

    # Check if registration for current fair is open
    now = datetime.now(tz=pytz.timezone(current_app.config['TIMEZONE']))
    if now < fair.registration_start.replace(tzinfo=pytz.timezone(current_app.config['TIMEZONE'])):
        formatted_registration_start_time = fair.registration_start.strftime("%A, %d %B %Y, %H:%M")
        return forbidden(f'Registration period opens on {formatted_registration_start_time}.')
    if fair.registration_end.replace(tzinfo=pytz.timezone(current_app.config['TIMEZONE'])) < now:
        return forbidden('Registration period has ended.')

    # Check if participation already exists
    existing_participation = Participation.query.filter_by(company_id=company.id, fair_id=fair.id).first()
    if existing_participation:
        return forbidden('Your company is already signed up.')

    # Lock current fair entry and check selected timeslot.
    fair = Fair.query.with_for_update().get(fair.id)
    selected_slot = get_timeslot_if_available(timeslot)

    if not selected_slot:
        return conflict('There are no booths available in your requested category at the desired time.')

    # get booth id of selected options
    booth = BoothOption.query.filter_by(fair_id=fair.id, booth_category=selected_slot['category']).first()
    if not booth:
        unprocessable_request('Could not find selected booth!')

    # Prepare entities to be inserted into the database.
    participation = Participation(company_id=company.id,
                                  fair_id=fair.id,
                                  created_time=now,
                                  updated_time=now,
                                  time_of_signup=now,
                                  accepted_participation_terms=accepted_participation_terms,
                                  accepted_permitted_items_terms=accepted_permitted_items_terms,
                                  message=message)

    db.session.add(participation)

    if selected_slot['day'] == 'FIRST':
        selected_day = FairDays.FIRST
    elif selected_slot['day'] == 'SECOND':
        selected_day = FairDays.SECOND
    elif selected_slot['day'] == 'THIRD':
        selected_day = FairDays.THIRD
    elif selected_slot['day'] == 'SECOND_AND_THIRD':
        selected_day = FairDays.SECOND_AND_THIRD
    else:
        selected_day = FairDays.BOTH

    if selected_slot['size'] == 'LARGE':
        selected_size = BoothSize.LARGE
    else:
        selected_size = BoothSize.SMALL

    participation.booths_booked.append(BoothBooked(booth_id=booth.id,
                                                   day=selected_day,
                                                   booth_size=selected_size,
                                                   table_type=table_type,
                                                   booth_location=None,
                                                   price=booth.get_booth_price(selected_day, selected_size)))

    for package_id in packages:
        package_option = PackageOption.query.get(package_id)
        if not package_option:
            return unprocessable_request(f'Invalid package option (id: {package_id}) selected.')
        else:
            participation.packages_booked.append(PackageBooked(package_id=package_option.id))

    # Set the correspondence e-mail address to the user's, unless it's already set
    if not company.correspondence_email_address:
        company.correspondence_email_address = g.user.email_address
        db.session.add(company)

    db.session.commit()

    return participation


@bp.route('/<p_id>', methods=['PATCH'])
@response(participation_fair_embedded_schema, code=200)
@login_required
def patch_participation(p_id):
    participation = Participation.query.get(p_id)
    if not participation:
        return not_found('Participation not found.')

    if participation.company_id != g.user.company_id:
        return forbidden()

    try:
        participation = participation_schema.load(request.json, instance=participation, unknown='EXCLUDE')
        Participation.persist(participation)
    except ValidationError as e:
        return unprocessable_request(e)

    return participation
