import json

from flask import Blueprint, jsonify, current_app
from sqlalchemy.orm.exc import NoResultFound

from .auth import api_token_required
from ..errorhandlers import not_found
from ..models import (
    Fair, CompanyProfile, PackageBooked, Participation, Company
)

bp = Blueprint('contractor', __name__, url_prefix='/contractor')


@bp.route('/company_info/<company_id>', methods=['GET'])
@api_token_required
def company_info(company_id):
    """
    Web hook for contractor
    :param company_id: company ID
    :return: 200 OK                 on success,
             404 Not Found          if requested company information does not exist
    """
    try:
        info = _get_company_info(company_id, Fair.get_current_fair())
    except NoResultFound as e:
        return not_found(e)

    return jsonify(info)


@bp.route('/companies/all', methods=['GET'])
@api_token_required
def all_companies():
    """
    Return a list of IDs and names of all companies participating in the current fair,
    plus the CV Check Company.

    :return: 200 OK                 on success
    """
    fair = Fair.get_current_fair()
    participations = Participation.query.filter_by(fair_id=fair.id).all()

    companies = [{
        'id': participation.company.id,
        'company_name': participation.company.company_name,
    } for participation in participations]

    return jsonify(companies)


def _get_company_info(company_id, fair: Fair):
    """Returns dict with company info needed for contractor."""
    company: Company = Company.query.get(company_id)
    if not company:
        raise NoResultFound

    participation: Participation = Participation.query.filter_by(company_id=company_id, fair_id=fair.id).first()

    if participation is not None:
        company_profile: CompanyProfile = CompanyProfile.query.filter_by(participation_id=participation.id).first()

        # Add data from company_profile if available
        if not company_profile:
            infos = {}
        else:
            infos = {
                'website': company_profile.website,
                'contact': company_profile.contact_for_students,
                'employees_ch': company_profile.employees_ch,
                'employees_world': company_profile.employees_world,
                'about': company_profile.about_us,
                'focus': company_profile.focus,
                'locations': company_profile.locations,
                'interested_in': [k for k, v in json.loads(company_profile.interesting_students).items() if v],
                'offers': [k for (k, v) in json.loads(company_profile.offers).items() if v],
            }

            if company_profile.logo:
                infos['logo'] = f'{current_app.config["BASE_URL"]}/company_profiles/{company_profile.id}/logo'

            if PackageBooked.has_ad(company_id):
                infos['has_ad'] = True

                if company_profile.advertisement:
                    infos['ad'] = \
                        f'{current_app.config["BASE_URL"]}/company_profiles/{company_profile.id}/advertisement'
            else:
                infos['has_ad'] = False
    else:
        infos = {}

    # Set general company information
    infos['name'] = company.company_name
    infos['contract_amiv_responsible'] = fair.kontakt_president
    infos['contract_city'] = company.billing_address_city
    infos['contract_email_address'] = company.correspondence_email_address
    infos['contract_partners'] = []
    infos['contract_postal_code'] = company.billing_address_postal_code
    infos['contract_street'] = company.billing_address_street

    if participation is None:
        infos['participation'] = {}
    else:
        infos['participation'] = {
            'packages': [{
                'id': package.package_option.id,
                'cost': package.package_option.price,
                'title_de': package.package_option.title_de,
                'title_en': package.package_option.title_en,
                'description_de': (package.package_option.description_long_de
                                   if package.package_option.description_long_de is not None
                                   else package.package_option.description_de),
                'description_en': (package.package_option.description_long_en
                                   if package.package_option.description_long_en is not None
                                   else package.package_option.description_en),
            } for package in participation.packages_booked],
            'booths': [{
                'id': booth.booth_option.id,
                'category': booth.booth_option.booth_category,
                'day': booth.day,
                'size': booth.booth_size,
                'tables': booth.table_type,
                'location': booth.booth_location,
                'price': booth.price,
            } for booth in participation.booths_booked],
            'price_total': participation.get_total_price(),
        }
    return infos
