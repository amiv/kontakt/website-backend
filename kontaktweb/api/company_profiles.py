import io
from flask import Blueprint, g, send_file, request
from marshmallow import ValidationError

from ..models import CompanyProfile, Fair, Participation
from ..schemas import company_profile_schema
from .auth import login_required
from ..errorhandlers import not_found, unprocessable_request, forbidden
from ..apihelper import response


bp = Blueprint('company_profiles', __name__, url_prefix='/company_profiles')


@bp.route('/current', methods=['GET'])
@response(company_profile_schema, code=200, many=True)
def get_current_profiles():
    """Get all company profiles for current fair."""
    fair = Fair.get_current_fair()
    participations = Participation.query.filter_by(fair_id=fair.id).all()

    return [p.company_profile for p in participations if p.company_profile is not None]


@bp.route('/current_for_user', methods=['GET'])
@response(company_profile_schema, code=200)
@login_required
def get_current_profile_for_user():
    """Get the current company profile for the authenticated user's company."""
    fair = Fair.get_current_fair()
    participation = Participation.query.filter_by(company_id=g.user.company_id,
                                                  fair_id=fair.id).first()
    if participation is None:
        return not_found('You are not yet signed up for this year\'s fair.')

    profile = participation.company_profile

    if profile is None:
        return not_found('You haven\'t created a profile yet for this year\'s fair.')
    return profile


@bp.route('/create', methods=['POST'])
@response(company_profile_schema, code=201)
@login_required
def create_profile():
    """Create a new company profile for the current fair."""
    fair = Fair.get_current_fair()
    company = g.user.company
    participation = Participation.query.filter_by(fair_id=fair.id, company_id=company.id).first()

    if not participation:
        unprocessable_request('Your company is not signed up for the current fair.')

    if participation.company_profile:
        unprocessable_request('Company profile already exists.')

    profile = participation.create_company_profile()
    return profile


@bp.route('/<p_id>', methods=['PATCH'])
@response(company_profile_schema, code=200)
@login_required
def patch_profile(p_id):
    """Update an existing company profile. Data must be submitted as form data!"""
    profile = CompanyProfile.query.get(p_id)
    if not profile:
        return not_found()

    if profile.participation.company_id != g.user.company_id:
        return forbidden()

    try:
        profile = company_profile_schema.load(request.form, instance=profile, unknown='EXCLUDE')

        if 'logo' in request.files:
            profile.logo = request.files['logo'].read()
            if len(profile.logo) > 16000000:
                return unprocessable_request('Logo file size may not exceed 16MB')
        if 'advertisement' in request.files:
            profile.advertisement = request.files['advertisement'].read()
            if len(profile.advertisement) > 16000000:
                return unprocessable_request('Advertisement file size may not exceed 16MB')

        CompanyProfile.persist(profile)
    except ValidationError as e:
        return unprocessable_request(e)

    return profile


@bp.route('/<p_id>', methods=['GET'])
@response(company_profile_schema, code=200)
def get_profile(p_id):
    """Returns the requested company profile object"""
    profile = CompanyProfile.query.get(p_id)
    if not profile:
        return not_found()
    return profile


@bp.route('/<p_id>/logo', methods=['GET'])
def get_logo(p_id):
    """Returns the logo file of the requested company profile"""
    profile = CompanyProfile.query.get(p_id)
    if not profile or not profile.logo:
        return not_found()

    return send_file(io.BytesIO(profile.logo), mimetype='image/svg+xml', cache_timeout=-1)


@bp.route('/<p_id>/advertisement', methods=['GET'])
def get_advertisement(p_id):
    """Returns the advertisement file of the requested company profile"""
    profile = CompanyProfile.query.get(p_id)
    if not profile or not profile.advertisement:
        return not_found()
    return send_file(io.BytesIO(profile.advertisement), mimetype='application/pdf', cache_timeout=-1)
