from flask import Blueprint
from sqlalchemy import or_

from ..apihelper import response
from ..schemas.fair import fair_schema
from ..schemas.fair_companies import fair_companies_schema
from ..models import Fair, Participation, Company, BoothBooked, CompanyProfile, FairDays, BoothOption
from ..database import db


bp = Blueprint('fairs', __name__, url_prefix='/fairs')


@bp.route('/current/companies', methods=['GET'])
@response(fair_companies_schema, code=200)
def get_current_fair_companies():
    """Get all companies participating on the current fair."""
    current_fair = Fair.get_current_fair()
    result = {}

    for day in [FairDays.FIRST, FairDays.SECOND]:
        participations = db.session.query(Participation) \
            .join(BoothBooked) \
            .join(BoothOption) \
            .join(Company) \
            .join(CompanyProfile) \
            .filter(Participation.fair_id == current_fair.id) \
            .filter(or_(BoothBooked.day == day, BoothBooked.day == FairDays.BOTH)) \
            .order_by(BoothOption.booth_category) \
            .all()

        items = []

        for participation in participations:
            company_name = None
            booth = None
            logo = None
            profile_id = None
            website = None

            if participation.company is not None:
                company_name = participation.company.company_name

            if participation.company_profile is not None:
                profile = participation.company_profile
                profile_id = profile.id
                website = profile.website
                logo = f'/company_profiles/{profile_id}/logo' if profile.logo else None

            for booth_booked in participation.booths_booked:
                if booth_booked.day == day or booth_booked.day == FairDays.BOTH:
                    booth = booth_booked.booth_location

            items.append({
                'logo': logo,
                'profile_id': profile_id,
                'company_name': company_name,
                'website': website,
                'booth': booth
            })

        result[day.name] = items

    return result


@bp.route('/current', methods=['GET'])
@response(fair_schema, code=200)
def get_current_fair():
    """Show information about this year's fair
    :return: 200 OK
    """
    return Fair.get_current_fair()


@bp.route('/past', methods=['GET'])
@response(fair_schema, code=200, many=True)
def get_fairs():
    query = db.session.query(Fair).filter_by(is_current=False)
    return query.all()
