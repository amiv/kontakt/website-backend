import functools

from flask import Blueprint, g, request, jsonify, session, abort, current_app, make_response
from flask_wtf.csrf import generate_csrf

from ..apihelper import response, kw_arguments
from ..errorhandlers import unauthorized, forbidden
from ..mail import send_password_reset_mail
from ..models import User
from ..schemas import user_schema
from ..schemas.auth import (
    login_schema, password_change_schema, password_set_schema, check_token_schema, request_password_reset_schema
)


bp = Blueprint('auth', __name__, url_prefix='/auth')


def login_required(view):
    """
    Decorate a view function to require a valid user. Otherwise, respond with 401 Unauthorized.
    :param view: The view function to be wrapped
    :return: The wrapped view function
    """

    @functools.wraps(view)
    def wrapped_view(**kwargs):
        user_id = session.get('id')

        if user_id is None:
            abort(401)
        user = User.query.filter_by(id=user_id).first()
        if not user:
            session.clear()
            abort(401)
        g.user = user
        return view(**kwargs)

    return wrapped_view


def api_token_required(view):
    """
    Decorate a view function to require a bearer token. Respond with
        - 401 Unauthorized if no token is provided
        - 403 Forbidden if the token is invalid
    :param view: The view function to be wrapped
    :return: The wrapped view function
    """

    @functools.wraps(view)
    def wrapped_view(**kwargs):
        api_token = request.headers.get('Authorization', None)

        if not api_token:
            return unauthorized('Missing "Authorization" header')

        if api_token not in current_app.config['API_KEYS'].values():
            return forbidden('Invalid API token')

        return view(**kwargs)

    return wrapped_view


@bp.route('/check', methods=['GET'])
@response(user_schema, code=200)
@login_required
def check():
    """
    Returns authenticated user object if user is logged in.
    :return: 200 OK             user object of authenticated user
    """
    return g.user


@bp.route('/getcsrf', methods=['GET'])
def get_csrf():
    """
    Request a new CSRF token
    :return: 200 OK   csrf token generated successfully
    """
    token = generate_csrf()
    response = 'CSRF token generated'
    return make_response(response, 200, {'X-CSRFToken': token})


@bp.route('/login', methods=['POST'])
@kw_arguments(login_schema, location='json')
@response(user_schema, code=200)
def login(email, password):
    """
    Log a user in with the given credentials
    Expects JSON object with fields "email" and "password".
    Must provide Content-Type: application/json!
    :return: 200 OK             user object of authenticated user
             400 Bad Request    error message
             401 Unauthorized   empty
    """
    user = User.login(email, password)

    if user is not None:
        session['id'] = user.id
        return jsonify(user_schema.dump(user)), 200
    else:
        return unauthorized('Wrong username and/or password.')


@bp.route('/logout', methods=['DELETE'])
@response(None, code=204)
def logout():
    """
    Log a user in with the given credentials
    Expects a JSON object with fields "email" and "password".
    :return: 204 No Content      empty
    """
    session.clear()
    return '', 204


@bp.route('/change_password', methods=['POST'])
@kw_arguments(password_change_schema)
@response(None, code=204)
@login_required
def change_password(old_password, new_password):
    """
    Set a new password for the authenticated user.
    Expects a JSON object with fields "old_password" and "new_password".
    :return: 204 No Content     on success,
             401 Unauthorized   if old_password is incorrect
    """
    if not g.user.check_password(old_password):
        return unauthorized('Old password is incorrect.')

    g.user.set_password(new_password)
    return ''


@bp.route('/set_password', methods=['POST'])
@kw_arguments(password_set_schema)
@response(None, code=204)
def set_password(email, token, new_password):
    """
    Set a new password for the user authenticated with the given token.
    :return: 204 No Content     on success,
             401 Unauthorized   if email/token is invalid or expired
    """
    user = User.get_by_email(email)

    if user is None or not user.is_token_valid(token):
        return unauthorized()

    user.set_password(new_password)
    return ''


@bp.route('/request_password_reset', methods=['POST'])
@kw_arguments(request_password_reset_schema)
@response(None, code=204)
def request_password_reset(email):
    """
    Request password reset email.
    :return: 204 No Content     on success
    """
    user = User.get_by_email(email)

    if user is not None:
        new_token_item = user.create_token()
        send_password_reset_mail(user, new_token_item.token)
    return ''


@bp.route('/check_token', methods=['POST'])
@kw_arguments(check_token_schema)
@response(None, code=200)
def check_token(email, token):
    """Check if the given email address / token combination is valid.
    :return: 200 OK            if valid,
             401 Unauthorized  if not valid.
    """
    user = User.get_by_email(email)

    if user is None or not user.is_token_valid(token):
        return unauthorized('Invalid token')
    return 'valid'
