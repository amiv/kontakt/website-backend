from flask import Blueprint

from ..schemas import timeslot_schema, booth_option_schema
from ..models import Fair, BoothOption
from ..apihelper import response
from ..dbhelper import get_timeslots


bp = Blueprint('booths', __name__, url_prefix='/booths')


@bp.route('/available', methods=['GET'])
@response(timeslot_schema, code=200, many=True)
def get_available_booths_for_current_fair():
    """Return list of dicts containing timeslot info.
    """
    return get_timeslots()


@bp.route('/options', methods=['GET'])
@response(booth_option_schema, code=200, many=True)
def get_booth_options_for_current_fair():
    """Return list of booth options for the current fair.
    """
    fair = Fair.get_current_fair()
    booth_options = BoothOption.query.filter_by(fair_id=fair.id)
    return booth_options
