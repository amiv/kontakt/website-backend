from flask import Blueprint

from ..schemas import package_option_schema
from ..models import Fair, PackageOption
from ..apihelper import response


bp = Blueprint('packages', __name__, url_prefix='/packages')


@bp.route('/current', methods=['GET'])
@response(package_option_schema, code=200, many=True)
def get_packages_for_current_fair():
    """Returns the package option objects for the current fair.
    """
    return PackageOption.query.filter_by(fair_id=Fair.get_current_fair().id).all()
