import json
import sentry_sdk
from flask import Flask, jsonify
from flask_migrate import Migrate
from flask_cors import CORS
from flask_wtf.csrf import CSRFProtect
from werkzeug.exceptions import HTTPException
from sentry_sdk.integrations.flask import FlaskIntegration

from . import models, schemas        # noqa: F401
from .api import auth, fairs, users, companies, participations, packages, booths, company_profiles, contractor
from .mail import mail
from .database import db, ma


# initialize flask app and configure
app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('config.py')
app.url_map.strict_slashes = False


# initialize ORM and schema validator
db.init_app(app)
ma.init_app(app)

# initialize mail library
mail.init_app(app)

# add database migration from flask-migrate
Migrate(app, db)

# add CORS handling
CORS(app)

# add CSRF protection
csrf = CSRFProtect(app)

# Enable Sentry, if configured
try:
    sentry_sdk.init(
        app.config['SENTRY_DSN'],
        integrations=[FlaskIntegration()],
    )
except KeyError:
    print("Sentry inactive: 'SENTRY_DSN' not found in config.", flush=True)


# load all blueprints
app.register_blueprint(auth.bp)
app.register_blueprint(users.bp)
app.register_blueprint(companies.bp)
app.register_blueprint(participations.bp)
app.register_blueprint(fairs.bp)
app.register_blueprint(packages.bp)
app.register_blueprint(booths.bp)
app.register_blueprint(company_profiles.bp)
app.register_blueprint(contractor.bp)


@app.after_request
def set_response_cache_headers(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '0'
    return response


@app.errorhandler(HTTPException)
def handle_http_exception(e):
    """Handler for all HTTPExceptions"""
    # start with the correct headers and status code from the error
    response = e.get_response()
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    print(e)
    return response


@app.errorhandler(Exception)
def handle_any_exception(e):
    """Handler for all non HTTPExceptions"""
    if isinstance(e, HTTPException):
        return e

    response_data = jsonify({
        "code": 500,
        "name": "Internal Server Error",
        "description": str(e),
    })
    return response_data, 500
