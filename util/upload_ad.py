"""Upload a company's advertisement for the current fair.

Usage: python ./upload_ad.py COMPANY_ID AD_PATH
where COMPANY_ID is the company's primary key in the `companies` table and AD_PATH is the path
to the PDF advertisement file. No file type checking is performed, so make sure that the file
is a valid PDF file before uploading it.
The script will fail if the company has not booked a package which includes an advertisement.
"""


def run():
    import os.path
    import sys

    from kontaktweb import app, db
    from kontaktweb.models.company import Company
    from kontaktweb.models.company_profile import CompanyProfile
    from kontaktweb.models.fair import Fair
    from kontaktweb.models.package_booked import PackageBooked

    if len(sys.argv) < 3:
        print('usage: upload_ad.py company_id ad_path')
        exit(1)

    company_id = sys.argv[1]
    ad_path = sys.argv[2]

    with app.app_context():
        fair_id = Fair.get_current_fair().id
        company_profile = CompanyProfile.query.filter_by(fair_id=fair_id, company_id=company_id).first()
        if not company_profile:
            print(f'Unable to find company profile for company_id={company_id}, fair_id={fair_id}')
            exit(1)

        if not PackageBooked.has_ad(company_id):
            print("Company's booking for current year does not include advertisement")
            exit(1)

        if not os.path.exists(ad_path):
            print(f'Unable to open ad file, ad_path={ad_path}')
            exit(1)

        company = Company.query.get(company_id)
        confirm = input(
            f'You are about to update the ad for "{company.company_name}" on database "{db.engine.url.database}". '
            'OK? [y/n]'
        )
        if confirm.lower() != 'y':
            print('Aborting')
            exit(0)

        with open(ad_path, 'rb') as ad_file:
            company_profile.advertisement = ad_file.read()
        db.session.commit()

        exit(0)


if __name__ == '__main__':
    import os
    import sys
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
    run()
