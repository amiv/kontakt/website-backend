"""Upload a company's logo for the current fair.

Usage: python ./upload_logo.py COMPANY_ID LOGO_PATH
where COMPANY_ID is the company's primary key in the `companies` table and LOGO_PATH is the path to the SVG logo file.
No file type checking is performed, so make sure that the file is a valid SVG file before uploading it.
"""


def run():
    import os.path
    import sys

    from kontaktweb import app, db
    from kontaktweb.models.company import Company
    from kontaktweb.models.company_profile import CompanyProfile
    from kontaktweb.models.fair import Fair

    if len(sys.argv) < 3:
        print('usage: upload_logo.py company_id logo_path')
        exit(1)

    company_id = sys.argv[1]
    logo_path = sys.argv[2]

    with app.app_context():
        fair_id = Fair.get_current_fair().id
        company_profile = CompanyProfile.query.filter_by(fair_id=fair_id, company_id=company_id).first()
        if not company_profile:
            print(f'Unable to find company profile for company_id={company_id}, fair_id={fair_id}')
            exit(1)

        if not os.path.exists(logo_path):
            print(f'Unable to open logo file, logo_path={logo_path}')
            exit(1)

        company = Company.query.get(company_id)
        confirm = input(
            f'You are about to update the logo for "{company.company_name}" on database "{db.engine.url.database}". '
            'OK? [y/n]'
        )
        if confirm.lower() != 'y':
            print('Aborting')
            exit(0)

        with open(logo_path, 'rb') as logo_file:
            company_profile.logo = logo_file.read()
        db.session.commit()

        exit(0)


if __name__ == '__main__':
    import os
    import sys
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
    run()
