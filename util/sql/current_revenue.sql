-- Show the revenue for the current fair based on the existing participations.
-- This includes both booths and packages.
DELIMITER $$
CREATE OR REPLACE PROCEDURE current_revenue()
BEGIN
    CREATE OR REPLACE TEMPORARY TABLE temp_products AS
        -- packages
    SELECT package_options.title_en AS product,
           package_options.price    AS unit_price,
           COUNT(*)                 AS count
    FROM packages_booked
             JOIN package_options on packages_booked.package_id = package_options.id
             JOIN fairs on package_options.fair_id = fairs.id
    WHERE fairs.is_current = 1
    GROUP BY packages_booked.package_id
    UNION ALL
    -- one day, small booth
    SELECT CONCAT('Category ', booth_category, ', one day, small booth') AS product,
           booth_options.price_one_day_small                             AS unit_price,
           COUNT(*)                                                      AS count
    FROM participations
             JOIN booth_options ON participations.booth_id = booth_options.id
    WHERE (participations.day = 'FIRST' OR participations.day = 'SECOND')
      AND participations.booth_size = 'SMALL'
    GROUP BY participations.booth_id
    UNION ALL
    -- one day, large booth
    SELECT CONCAT('Category ', booth_category, ', one day, large booth') AS product,
           booth_options.price_one_day_large                             AS unit_price,
           COUNT(*)                                                      AS count
    FROM participations
             JOIN booth_options ON participations.booth_id = booth_options.id
    WHERE (participations.day = 'FIRST' OR participations.day = 'SECOND')
      AND participations.booth_size = 'LARGE'
    GROUP BY participations.booth_id
    UNION ALL
    -- two days, small booth
    SELECT CONCAT('Category ', booth_category, ', two days, small booth') AS product,
           booth_options.price_two_days_small                             AS unit_price,
           COUNT(*)                                                       AS count
    FROM participations
             JOIN booth_options ON participations.booth_id = booth_options.id
    WHERE participations.day = 'BOTH'
      AND participations.booth_size = 'SMALL'
    GROUP BY participations.booth_id
    UNION ALL
    -- two days, large booth
    SELECT CONCAT('Category ', booth_category, ', two days, large booth') AS product,
           booth_options.price_two_days_large                             AS unit_price,
           COUNT(*)                                                       AS count
    FROM participations
             JOIN booth_options ON participations.booth_id = booth_options.id
    WHERE participations.day = 'BOTH'
      AND participations.booth_size = 'LARGE'
    GROUP BY participations.booth_id;

    CREATE OR REPLACE TEMPORARY TABLE temp_products_aggr AS
    SELECT product            AS product,
           unit_price * count AS revenue
    FROM temp_products;

    CREATE OR REPLACE TEMPORARY TABLE temp_products_sum AS
    SELECT SUM(revenue) AS total FROM temp_products_aggr;

    SELECT product AS product,
           revenue AS revenue
    FROM temp_products_aggr
    UNION ALL
    SELECT 'TOTAL' AS product,
           total   AS revenue
    FROM temp_products_sum;

    DROP TEMPORARY TABLE temp_products_sum;
    DROP TEMPORARY TABLE temp_products_aggr;
    DROP TEMPORARY TABLE temp_products;
END;
$$
DELIMITER ;
