CREATE OR REPLACE VIEW `billing_info` AS
select `participations`.`time_of_signup`          AS `time_of_signup`,
       `booths_booked`.`day`                      AS `day`,
       `booths_booked`.`booth_size`               AS `booth_size`,
       `booths_booked`.`table_type`               AS `table_type`,
       `participations`.`message`                 AS `message`,
       `booths_booked`.`booth_location`           AS `booth_location`,
       `package_options`.`title_en`               AS `package_name`,
       `package_options`.`price`                  AS `package_price`,
       `booth_options`.`booth_category`           AS `booth_category`,
       `participations`.`booth_price`             AS `booth_price`,
       `companies`.`company_name`                 AS `company_name`,
       `companies`.`correspondence_email_address` AS `correspondence_email_address`,
       `companies`.`billing_recipient`            AS `billing_recipient`,
       `companies`.`billing_address_street`       AS `billing_address_street`,
       `companies`.`billing_address_postal_code`  AS `billing_address_postal_code`,
       `companies`.`billing_address_city`         AS `billing_address_city`,
       `companies`.`billing_address_country`      AS `billing_address_country`,
       `companies`.`billing_comment`              AS `billing_comment`
from `participations`
         join `fairs` on `participations`.`fair_id` = `fairs`.`id`
         left join `packages_booked` on `participations`.`id` = `packages_booked`.`participation_id`
         left join `package_options` on `packages_booked`.`package_id` = `package_options`.`id`
         left join `booths_booked` on `participations`.`id` = `booths_booked`.`participation_id`
         left join `booth_options` on `booths_booked`.`booth_id` = `booth_options`.`id`
         join `companies` on `participations`.`company_id` = `companies`.`id`
where `fairs`.`is_current` = 1
group by `participations`.`id`
