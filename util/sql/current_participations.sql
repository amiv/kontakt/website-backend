-- Show the participations for the current fair by day
-- param: days: enum ('FIRST', 'SECOND', 'ALL')
--              whether to show participations for the first fair day, second fair day, or all participations.
DELIMITER $$
CREATE OR REPLACE PROCEDURE current_participations(IN days enum ('FIRST', 'SECOND', 'ALL'))
BEGIN

    CREATE OR REPLACE TEMPORARY TABLE temp_all_current_participations
    (
        company_name                   VARCHAR(100),
        category                       VARCHAR(1),
        booth_size                     VARCHAR(10),
        day                            VARCHAR(10),
        accepted_participation_terms   TINYINT,
        accepted_permitted_items_terms TINYINT
    )
    AS (
        SELECT companies.company_name                        AS company_name,
               booth_options.booth_category                  AS category,
               participations.booth_size                     AS booth_size,
               participations.day                            AS day,
               participations.accepted_participation_terms   AS accepted_participation_terms,
               participations.accepted_permitted_items_terms AS accepted_permitted_items_terms
        FROM companies
                 JOIN participations on companies.id = participations.company_id
                 JOIN booth_options on participations.booth_id = booth_options.id
                 JOIN fairs on booth_options.fair_id = fairs.id
        WHERE fairs.is_current = 1
    );

    CASE days
        WHEN 'FIRST' THEN SELECT * FROM temp_all_current_participations WHERE (day = 'FIRST' OR day = 'BOTH');
        WHEN 'SECOND' THEN SELECT * FROM temp_all_current_participations WHERE (day = 'SECOND' OR day = 'BOTH');
        ELSE SELECT * FROM temp_all_current_participations;
        END CASE;

    DROP TEMPORARY TABLE temp_all_current_participations;

END;
$$
DELIMITER ;
