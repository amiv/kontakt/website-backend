-- Show the number of booked packages for the current fair
DELIMITER $$
CREATE OR REPLACE PROCEDURE current_packages()
BEGIN
    SELECT package_options.title_en AS package,
           COUNT(*)                 AS count
    FROM packages_booked
             JOIN package_options on packages_booked.package_id = package_options.id
             JOIN fairs on package_options.fair_id = fairs.id
    WHERE fairs.is_current = 1
    GROUP BY packages_booked.package_id;
END;
$$
DELIMITER ;
