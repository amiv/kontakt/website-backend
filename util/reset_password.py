"""Reset a user's password to a randomly-generated one.

Usage: python reset_password.py EMAIL
where EMAIL is the e-mail address of the user whose password is to be reset.
"""


def run():
    import secrets
    import sys

    from kontaktweb import app, db
    from kontaktweb.models.user import User

    NBYTES = 8

    if len(sys.argv) < 2:
        print('usage: reset_password.py email')
        exit(1)

    email = sys.argv[1]

    with app.app_context():
        user = User.query.filter_by(email_address=email).first()
        if not user:
            print(f'Unable to find user with e-mail address {email}')
            exit(1)

        confirm = input(
            f'You are about to reset the password of user "{email}" on database "{db.engine.url.database}". OK? [y/n]'
        )
        if confirm.lower() != 'y':
            print('Aborting')
            exit(0)

        password = secrets.token_urlsafe(NBYTES)
        user.set_password(password)
        print(f'Reset password for user "{email}" to "{password}"')
        exit(0)


if __name__ == '__main__':
    import os
    import sys
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
    run()
