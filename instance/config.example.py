# Example Configuration File
from datetime import timedelta

DEBUG = False
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = False

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://<username>:<password>@<host>/<db-name>'
SECRET_KEY = 'replace me (24 random bytes)'

# API Keys
API_KEYS = {
    'Contractor': '<KEY_GOES_HERE>',
}

TIMEZONE = 'Europe/Zurich'
USER_TOKEN_TIMEOUT = timedelta(hours=4)
SESSION_COOKIE_DOMAIN = 'kontakt.amiv.ethz.ch'
REMEMBER_COOKIE_HTTPONLY = True
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_SAMESITE = 'Lax'

BASE_URL = 'https://api.kontakt.amiv.ethz.ch'
WEBSITE_URL = 'https://kontakt.amiv.ethz.ch'
CONTACT_EMAIL = 'kontaktmesse@amiv.ethz.ch'
CONTRACTOR_PREVIEW_URL = 'https://contractor.amiv.ethz.ch/fairguidepage/pdf/{companyId}'

# List of origins which are allowed to access the resources.
CORS_ORIGINS = ['https://kontakt.amiv.ethz.ch']
CORS_ALLOW_HEADERS = ['X-CSRFToken']

SERVER_NAME = 'api.kontakt.amiv.ethz.ch'

MAIL_SERVER = 'localhost'
MAIL_PORT = 25
MAIL_USE_TLS = True
MAIL_USERNAME = None
MAIL_PASSWORD = None
MAIL_DEFAULT_SENDER = 'AMIV Kontakt <noreply@amiv.ethz.ch>'
