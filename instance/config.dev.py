# Example Configuration File
from datetime import timedelta

DEBUG = True
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = False

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://kontaktweb:kontaktweb@db/kontaktweb'
SECRET_KEY = 'zMdmnVTcpqF6daQvHtwUiyDgd-oJRg'

# API Keys
API_KEYS = {
    'Contractor': '9Fti1a9gfdgefv8jr63tc3trq7u3t9d7hwtb',
}

TIMEZONE = 'Europe/Zurich'
USER_TOKEN_TIMEOUT = timedelta(hours=4)
REMEMBER_COOKIE_HTTPONLY = True
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = False
SESSION_COOKIE_SAMESITE = 'strict'
SESSION_COOKIE_DOMAIN = False

BASE_URL = 'http://localhost:3000/api'
WEBSITE_URL = 'http://localhost:3000'
CONTACT_EMAIL = 'kontaktmesse@amiv.ethz.ch'

MAIL_SERVER = 'smtp'
MAIL_PORT = 1025
MAIL_USE_TLS = False
MAIL_USERNAME = None
MAIL_PASSWORD = None
MAIL_DEFAULT_SENDER = 'AMIV Kontakt <noreply@amiv.ethz.ch>'
