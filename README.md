# AMIV Kontakt Website Backend

This is the home of the amiv Kontakt website backend.

## 🚀 Quick start

1. **Development with Devcontainer (VSCode)**

   You can run the prepared development environment with docker and VSCode without the need to install anything project-specific on your system.

   Requirements:

   * *(Windows only)* [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/)
   * [Docker Engine](https://docs.docker.com/engine/install/)
   * [Visual Studio Code](https://code.visualstudio.com/)(VSCode)
   * [Remote Development Extension Pack for VSCode](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)

   Open the project directory with the prepared devcontainer by clicking the green button in the left button corner of the VSCode window and select `Remote-Containers: Open Folder in Container...`.

   On startup, `pip install -r requirements.txt` is run automatically.
   You can start the development process with the prepared Run Configuration `Python: Flask` or manually from the terminal with `flask run`.

   > Please note that you may have to initialize the database first.

   You can visit the api now at http://localhost:5000

2. **Development without Docker**

   Navigate into the project directory, install all dependencies and run the development server using the following commands:

   ```shell
   # Create and activate virtual environment
   python -m venv env
   source env/bin/activate
 
   # Install requirements
   pip install -r requirements.txt
 
   # Run development server, available at localhost:5000 per default
   export FLASK_APP=kontaktweb
   export FLASK_ENV=development
   flask run
   ```

   > Before running the development server, you have to create a configuration file `instance/config.py`. An example file is provided as `instance/config.example.py`.
   >
   > Please note that you also need to run a MySQL database server and a (mock) SMTP server locally.
   > You can find more details for the configuration in the file `.devcontainer/docker-compose.yml`.

   You can visit the API now at http://localhost:5000

3. **Database: Initialize, upgrade Schema & Create Migration Files**

   > In Visual Studio Code, you can just run the configured tasks `Flask: Upgrade Database Schema` and `Flask: Create Database Migration Files`.

   For initializing and migrating the database schema to the most recent version, run the following command in the terminal:

   ```shell
   flask db upgrade
   ```

   If you did some changes to the database model classes, just run the following command to create new database migration files:

   ```shell
   flask db migrate
   ```
   
   **IMPORTANT NOTE:** Do NOT change migration files after the have been committed! Otherwise, you are in big trouble.

  3. **Linting & Formatting**

     > In Visual Studio Code, you can just run the configured tasks `Flake8: Linting Source Files`.

     Run the linter with the following command:

     ```shell
     flake8
     ```

  4. **Running Tests**

     Let's not kid ourselves. We don't have tests.
  
  5. **Update Dependencies**

     > In Visual Studio Code, you can just run the configured tasks `Pip: Update Dependencies`.

     All required dependencies must be specified in the file `requirements.in` (without version numbers!).

     For updating the actual dependency list, run the following command:

     ```shell
     pip-compile --output-file requirements.txt requirements.in
     ```

     > Please note that you may need to run `pip install pip-compile` first, if you are not using the prepared devcontainer.

## 🧐 What's inside?

A quick look at the most important files and directories of the project.

```ascii
.
├── .devcontainer/
├── .vscode/
├── instance/
├── migrations/
├── util/
├── .flake8
├── requirements.in
├── requirements.txt
├── run_dev.py
├── run_prod.py
└── kontaktweb
    ├── models
    ├── schemas
    ├── templates
    └── ...
```

TODO: describle file structure

## 💫 Deployment

The Kontakt Website is available as a Docker image with the name `amiveth/kontakt-website`.

> The image is built automatically during the CI/CD Pipeline.

You only need to install Docker, nothing else is required.

```shell
# Create docker config based on configuration file.
docker config create kontaktwebsite_config config.py

# Map port 80 (host) to 8080 (container)
docker service create \
    --name kontakt-website  -p 80:8080 --network backend \
    --config source=kontaktwebsite_config,target=/kontaktwebsite/instance/config.py \
    amiveth/kontakt-website
```

## ⚙ Technologies

### Frontend Frameworks & Libraries

- [Flask](https://flask.palletsprojects.com/)
- [SQLAlchemy](https://www.sqlalchemy.org/)

### Build Tools

- [pip](https://pip.pypa.io/en/stable/)

### Development Tools

VS Code is the recommended IDE. It also allows to use the prepared devcontainer for local development.
